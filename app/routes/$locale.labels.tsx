import { LoaderArgs } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import { useState } from "react";
import { useTranslations } from "use-intl";

import ContentWrapper from "~/components/ContentWrapper";
import Description from "~/components/Description";
import LabelListItem from "~/components/LabelListItem";
import Loader from "~/components/Loader";

import AutoLayout from "../components/AutoLayout";
import Button from "../components/Button";
import CardTitle from "../components/CardTitle";
import IconButton from "../components/IconButton";
import LabelCreator from "../components/LabelCreator";
import PageWrapper from "../components/PageWrapper";
import Spacer from "../components/Spacer";
import TextInput from "../components/TextInput";
import { fetchLabels } from "../composables/use_label_list";
import ILabel from "../types/label";
import { deleteLabels } from "../util/mutations/label";

export const loader = async (_: LoaderArgs) => {
  const labels = await fetchLabels();

  return {
    labels,
  };
};

export default function LabelsPage() {
  const t = useTranslations();
  const { revalidate, state } = useRevalidator();

  const [query, setQuery] = useState("");
  const { labels } = useLoaderData<typeof loader>();

  const loading = state === "loading";

  const normalizedFilter = query.toLowerCase().trim();

  function filterLabel(label: ILabel, index: number) {
    if (normalizedFilter.length === 0) {
      return true;
    }

    const name = `${label.name?.toLowerCase()}${label.aliases?.join("").toLowerCase()}`;

    if (!name) {
      return true;
    }

    return name.indexOf(normalizedFilter) >= 0;
  }

  const filteredLabels = labels.filter(filterLabel);

  const numItems: number = filteredLabels.length;

  return (
    <PageWrapper title={t("foundLabels", { numItems })}>
      <AutoLayout className="max-w-3xl w-full mx-auto">
        <div className="flex items-center">
          <CardTitle>{t("foundLabels", { numItems })}</CardTitle>
          <Spacer />
        </div>
        <Description>
          Labels categorize entities, and can be used to filter content. Label aliases allow label
          inheritance (labels being applied even if they are not matched directly, but indirectly
          through aliases).
        </Description>
        <div className="flex items-center">
          <LabelCreator onCreate={revalidate} />
        </div>
        <AutoLayout layout="h" gap={10}>
          <TextInput
            className="max-w-[160px]"
            value={query}
            onChange={setQuery}
            placeholder={t("action.findLabels")}
          />
          <Spacer />
          <Button loading={loading} onClick={revalidate}>
            {t("action.refresh")}
          </Button>
        </AutoLayout>
        <ContentWrapper
          loader={
            <div className="flex justify-center">
              <Loader />
            </div>
          }
          loading={loading}
          noResults={labels.length === 0}
        >
          <div className="shadow-lg rounded-xl overflow-hidden">
            {filteredLabels.map((label, index) => (
              <LabelListItem
                id={label._id}
                {...label}
                index={index}
                onUpdate={revalidate}
                onDelete={revalidate}
              />
            ))}
          </div>
        </ContentWrapper>
      </AutoLayout>
    </PageWrapper>
  );
}
