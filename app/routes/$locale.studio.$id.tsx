import { LoaderArgs, redirect } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import Divider from "~/components/Divider";
import FileInput from "~/components/FileInput";
import StudioEditor from "~/components/studio_details/StudioEditor";
import Text from "~/components/Text";
import { useLocaleNavigate } from "~/composables/use_navigate";
import { uploadImage } from "~/util/mutations/image";
import { removeStudio, setStudioThumbnail } from "~/util/mutations/studio";

import AutoLayout from "../components/AutoLayout";
import Card from "../components/Card";
import CardSection from "../components/CardSection";
import CardTitle from "../components/CardTitle";
import Description from "../components/Description";
import Flex from "../components/Flex";
import IconButton from "../components/IconButton";
import LabelGroup from "../components/LabelGroup";
import LocaleLink from "../components/Link";
import ListWrapper from "../components/ListWrapper";
import PageWrapper from "../components/PageWrapper";
import Rating from "../components/Rating";
import Spacer from "../components/Spacer";
import SceneList from "../components/studio_details/SceneList";
import StudioCard from "../components/StudioCard";
import { studioCardFragment } from "../fragments/studio";
import { IStudio } from "../types/studio";
import { graphqlQuery } from "../util/gql";
import { imageUrl } from "../util/thumbnail";

export const loader = async ({ params }: LoaderArgs) => {
  const q = `
  query ($id: String!) {
    getStudioById(id: $id) {
      ...StudioCard
      addedOn
      description
      aliases
      substudios {
        ...StudioCard
      }
    }
  }
  ${studioCardFragment}
  `;

  const { getStudioById } = await graphqlQuery<{
    getStudioById: IStudio;
  }>(q, {
    id: params.id,
  });

  if (!getStudioById) {
    throw redirect("/studios");
  }

  return {
    studio: getStudioById,
  };
};

export default function StudioPage() {
  const { studio } = useLoaderData<typeof loader>();
  const t = useTranslations();
  const navigate = useLocaleNavigate();
  const { revalidate } = useRevalidator();
  const [isUploading, setUploading] = useState(false);

  return (
    <PageWrapper key={studio._id} title={studio.name}>
      <AutoLayout className="w-full overflow-hidden">
        <div className="mt-5 flex justify-center">
          <AutoLayout className="max-w-[300px] w-full overflow-hidden">
            {studio.thumbnail && (
              <img width="100%" src={imageUrl(studio.thumbnail?._id)} alt="Studio Logo" />
            )}
            <span className="text-center font-semibold text-gray-800 dark:text-gray-200">
              <FileInput
                loading={isUploading}
                onChange={async ([file]) => {
                  if (!file) {
                    return;
                  }

                  try {
                    setUploading(true);

                    const image = await uploadImage({
                      blob: file,
                      name: `${studio.name} (logo)`,
                    });
                    await setStudioThumbnail(studio._id, image._id);

                    revalidate();
                  } catch (error) {
                  } finally {
                    setUploading(false);
                  }
                }}
              >
                {t("action.setLogo")}
              </FileInput>
            </span>
          </AutoLayout>
        </div>
        <div className="flex justify-center items-center">
          {!!studio.parent && (
            <div className="flex justify-center gap-2 items-center">
              <span className="font-semibold text-gray-700 dark:text-gray-300">Part of:</span>{" "}
              <LocaleLink
                to={`/studio/${studio.parent._id}`}
                className="flex items-center hover:opacity-80 transition-all"
              >
                {studio.parent.thumbnail ? (
                  <img
                    width="128"
                    src={imageUrl(studio.parent.thumbnail?._id)}
                    alt="Parent Studio Logo"
                  />
                ) : (
                  <span className="font-semibold text-lg dark:text-white">
                    {studio.parent.name}
                  </span>
                )}
              </LocaleLink>
            </div>
          )}
        </div>
        <AutoLayout gap={10} layout="v" className="w-full mx-auto max-w-3xl">
          <CardTitle>{studio.name}</CardTitle>
          <Card className="w-full">
            {/* Actions */}
            <Flex layout="h" className="gap-2">
              <Spacer />
              <StudioEditor onEdit={revalidate} studio={studio} />
              <IconButton
                onClick={async () => {
                  if (confirm("Really delete?")) {
                    await removeStudio(studio._id);
                    navigate("/studios");
                  }
                }}
                Icon={DeleteIcon}
              />
            </Flex>
            <Divider />
            <CardTitle>{t("general")}</CardTitle>
            <AutoLayout>
              {!!studio.aliases?.length && (
                <CardSection title={t("aliases")}>
                  <ul className="leading-7">
                    {studio.aliases.map((x) => (
                      <li key={x}>{x}</li>
                    ))}
                  </ul>
                </CardSection>
              )}
              {studio.description && (
                <CardSection title={t("description")}>
                  <Description>{studio.description}</Description>
                </CardSection>
              )}
              <CardSection title={t("avgRating")}>
                <Rating value={studio.averageRating} readonly />
              </CardSection>
              <CardSection title={t("numScenes")}>
                <Text className="text-sm">
                  {studio.numScenes} {t("scene", { numItems: studio.numScenes })}
                </Text>
              </CardSection>
              <CardSection title={t("addedToLibrary")}>
                <Text className="text-sm">{new Date(studio.addedOn).toLocaleString()}</Text>
              </CardSection>
              {!!studio.labels.length && (
                <CardSection title="Labels">
                  {/* TODO: use inline editor like on scene page,
                      not studioEditor
                  */}
                  <LabelGroup limit={999} value={studio.labels} />
                </CardSection>
              )}
            </AutoLayout>
          </Card>
          {!!studio.substudios.length && (
            <AutoLayout>
              <CardTitle>Substudios</CardTitle>
              <ListWrapper loading={false} noResults={!studio.substudios.length}>
                {studio.substudios.map((studio) => (
                  <StudioCard
                    key={studio._id}
                    onFav={revalidate}
                    onBookmark={revalidate}
                    studio={studio}
                  />
                ))}
              </ListWrapper>
            </AutoLayout>
          )}
          <SceneList studioId={studio._id} substudios={studio.substudios} />
        </AutoLayout>
      </AutoLayout>
    </PageWrapper>
  );
}
