import { LoaderArgs, redirect } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import CopyIcon from "mdi-react/ContentCopyIcon.js";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import prettyBytes from "pretty-bytes";
import { useEffect, useState } from "react";
import { useTranslations } from "use-intl";

import CustomFieldForm from "~/components/CustomFieldForm";
import CustomFieldGrid from "~/components/CustomFieldGrid";
import SceneEditor from "~/components/scene_details/SceneEditor";
import SkeletonCard from "~/components/SkeletonCard";
import Spacer from "~/components/Spacer";
import Window from "~/components/Window";
import { useLocaleNavigate } from "~/composables/use_navigate";
import { useSceneGrid } from "~/composables/use_scene_grid";
import { useWindow } from "~/composables/use_window";

import ActorCard from "../components/ActorCard";
import AutoLayout from "../components/AutoLayout";
import BookmarkIconButton from "../components/BookmarkIconButton";
import Button from "../components/Button";
import Card from "../components/Card";
import CardSection from "../components/CardSection";
import CardTitle from "../components/CardTitle";
import Description from "../components/Description";
import Divider from "../components/Divider";
import FavoriteIconButton from "../components/FavoriteIconButton";
import IconButton from "../components/IconButton";
import { SelectableLabel } from "../components/LabelDropdownChoice";
import LabelGroup from "../components/LabelGroup";
import LocaleLink from "../components/Link";
import ListWrapper from "../components/ListWrapper";
import MarkerCreator from "../components/MarkerCreator";
import MovieCard from "../components/MovieCard";
import PageWrapper from "../components/PageWrapper";
import Rating from "../components/Rating";
import FFProbeButton from "../components/scene_details/FFProbeButton";
import SceneDetailsImageList from "../components/scene_details/ImageList";
import MarkerList from "../components/scene_details/MarkerList";
import SimilarScenesList from "../components/scene_details/SimilarScenesList";
import SceneThumbnailEditor from "../components/scene_details/ThumbnailEditor";
import VideoPlayer from "../components/scene_details/VideoPlayer";
import Tabs from "../components/Tabs";
import Text from "../components/Text";
import { useTitleColor } from "../composables/use_title_color";
import { useVideoControls } from "../composables/use_video_control";
import { scenePageFragment } from "../fragments/scene";
import { IScene } from "../types/scene";
import { graphqlQuery } from "../util/gql";
import {
  bookmarkScene,
  favoriteScene,
  rateScene,
  removeLabel,
  removeScene,
  runScenePlugins,
  screenshotScene,
  setSceneThumbnail,
  unwatchScene,
  updateCustomFields,
  updateLabels,
  watchScene,
} from "../util/mutations/scene";
import { formatDuration } from "../util/string";
import { thumbnailUrl } from "../util/thumbnail";

/* const queryParser = buildQueryParser({
  t: {
    default: null,
  },
}); */

type TabState = "actors" | "markers" | "movies" | "images" | "similar";

export const loader = async ({ params }: LoaderArgs) => {
  /* const { t } = queryParser.parse(ctx.query); */

  const q = `
  query ($id: String!) {
    getSceneById(id: $id) {
      ...ScenePage
    }
  }
  ${scenePageFragment}
  `;

  const { getSceneById } = await graphqlQuery<{
    getSceneById: IScene & { fileExists: boolean };
  }>(q, {
    id: params.id,
  });

  if (!getSceneById) {
    throw redirect("/scenes");
  }

  return {
    scene: getSceneById,
    /*   startAtPosition: t, */
  };
};

function timeAgo(timestamp: number) {
  let value;
  const diff = (Date.now() - timestamp) / 1000;
  const minutes = Math.floor(diff / 60);
  const hours = Math.floor(minutes / 60);
  const days = Math.floor(hours / 24);
  const months = Math.floor(days / 30);
  const years = Math.floor(months / 12);
  const rtf = new Intl.RelativeTimeFormat(undefined, { numeric: "auto" });

  if (years > 0) {
    value = rtf.format(0 - years, "year");
  } else if (months > 0) {
    value = rtf.format(0 - months, "month");
  } else if (days > 0) {
    value = rtf.format(0 - days, "day");
  } else if (hours > 0) {
    value = rtf.format(0 - hours, "hour");
  } else if (minutes > 0) {
    value = rtf.format(0 - minutes, "minute");
  } else {
    value = rtf.format(0 - diff, "second");
  }
  return value;
}

export default function ScenePage() {
  const { scene } = useLoaderData<typeof loader>();
  const t = useTranslations();
  const titleColor = useTitleColor(scene.thumbnail?.color);

  const { revalidate } = useRevalidator();
  const navigate = useLocaleNavigate();

  const [labels, setLabels] = useState<SelectableLabel[]>(scene.labels);
  const [pausedByMarker, setPausedByMarker] = useState(false);

  const [watches, setWatches] = useState<number[]>(scene.watches);
  const [watchLoader, setWatchLoader] = useState(false);
  const [screenshotLoader, setScreenshotLoader] = useState(false);

  const [pluginLoader, setPluginLoader] = useState(false);

  const { gridUrl, gridLoader, showGrid } = useSceneGrid(scene._id);

  const { startPlayback, currentTime, paused } = useVideoControls();

  const [activeTab, setActiveTab] = useState<TabState>("actors");

  // NOTE: useMemo seems to have problems when adding new markers
  // const sortedMarkers = useMemo(() => markers.slice().sort((a, b) => a.time - b.time), [markers]);
  const sortedMarkers = scene.markers.slice().sort((a, b) => a.time - b.time);

  const hasRatedAllMarkers = scene.markers.every((mk) => mk.rating > 0) && scene.markers.length > 0;

  const averageRatingByMarkers = scene.markers.length
    ? scene.markers.reduce((acc, mk) => acc + mk.rating, 0) / scene.markers.length
    : 0;

  const customFieldFormWindow = useWindow();
  const [customFields, setCustomFields] = useState(() =>
    scene.resolvedCustomFields.reduce((acc, { field, value }) => {
      acc[field._id] = value;
      return acc;
    }, {} as Record<string, any>)
  );

  // NOTE: ugly
  useEffect(() => {
    setLabels(scene.labels);
    setWatches(scene.watches);
    setCustomFields(
      scene.resolvedCustomFields.reduce((acc, { field, value }) => {
        acc[field._id] = value;
        return acc;
      }, {} as Record<string, any>)
    );
  }, [scene]);

  async function toggleFav(): Promise<void> {
    const newValue = !scene.favorite;
    await favoriteScene(scene._id, newValue);
    revalidate();
  }

  async function toggleBookmark(): Promise<void> {
    const newValue = scene.bookmark ? null : new Date();
    await bookmarkScene(scene._id, newValue);
    revalidate();
  }

  async function changeRating(rating: number): Promise<void> {
    await rateScene(scene._id, rating);
    revalidate();
  }

  async function _watchScene(): Promise<void> {
    try {
      setWatchLoader(true);
      const watches = await watchScene(scene._id);
      setWatches(watches);
    } catch (error) {
    } finally {
      setWatchLoader(false);
    }
  }

  async function _unwatchScene(): Promise<void> {
    try {
      setWatchLoader(true);
      const watches = await unwatchScene(scene._id);
      setWatches(watches);
    } catch (error) {
    } finally {
      setWatchLoader(false);
    }
  }

  async function handleRunScenePlugins() {
    try {
      setPluginLoader(true);
      await runScenePlugins(scene._id);
      revalidate();
    } catch (error) {
      console.error(error);
    } finally {
      setPluginLoader(false);
    }
  }

  async function handleScreenshotScene() {
    try {
      setScreenshotLoader(true);
      await screenshotScene(scene._id, currentTime);
      // revalidate();
    } catch (error) {
      console.error(error);
    } finally {
      setScreenshotLoader(false);
    }
  }

  async function deleteLabel(id: string) {
    await removeLabel(scene._id, id);
    setLabels(labels.filter((label) => label._id !== id));
  }

  async function doDeleteScene() {
    if (
      window.confirm("Really delete? This will also delete the associated video file and images")
    ) {
      await removeScene(scene._id);
      navigate("/scenes");
    }
  }

  return (
    <PageWrapper key={scene._id} padless title={scene.name}>
      <AutoLayout>
        <VideoPlayer
          scene={scene}
          // startAtPosition={startAtPosition}
          duration={scene.meta.duration}
          markers={scene.markers.map((marker) => ({
            ...marker,
            time: marker.time / scene.meta.duration,
            thumbnail: thumbnailUrl(marker.thumbnail?._id),
          }))}
          poster={thumbnailUrl(scene.thumbnail?._id)}
          src={`/api/media/scene/${scene._id}`}
        />
        <div className="flex gap-3 justify-center">
          <div className="w-full max-w-[1000px] py-0 px-2">
            <AutoLayout>
              <div
                style={{
                  color: titleColor,
                }}
                className="text-xl font-semibold text-gray-800 dark:text-gray-100"
              >
                {scene.name}
              </div>
              <Card>
                {/* ACTION BAR */}
                <div className="flex items-center gap-2 py-1">
                  <FavoriteIconButton value={scene.favorite} onClick={toggleFav} />
                  <BookmarkIconButton value={scene.bookmark} onClick={toggleBookmark} />
                  <MarkerCreator
                    onOpen={() => {
                      const videoEl = document.getElementById(
                        "video-player"
                      ) as HTMLVideoElement | null;
                      if (videoEl) {
                        videoEl.pause();
                        if (!paused) {
                          setPausedByMarker(true);
                        }
                      }
                    }}
                    sceneId={scene._id}
                    actors={scene.actors}
                    onCreate={async () => {
                      const videoEl = document.getElementById(
                        "video-player"
                      ) as HTMLVideoElement | null;
                      if (videoEl && pausedByMarker) {
                        videoEl.play().catch(() => { });
                      }
                      setPausedByMarker(false);
                      revalidate();
                    }}
                  />
                  <Spacer />
                  <SceneEditor scene={scene} onEdit={revalidate} />
                  <IconButton
                    Icon={DeleteIcon}
                    onClick={doDeleteScene}
                    className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
                  />
                </div>
                <Divider />
                {/* MAIN INFO */}
                <CardTitle>{t("general")}</CardTitle>
                <div
                  className="grid gap-3"
                  style={{
                    gridTemplateColumns: "repeat(auto-fit, minmax(250px, 1fr))",
                  }}
                >
                  <AutoLayout>
                    {!!scene.studio && (
                      <CardSection title="Studio">
                        {scene.studio.thumbnail ? (
                          <div className="flex">
                            <LocaleLink className="hover" to={`/studio/${scene.studio._id}`}>
                              <img
                                className="max-h-[120px] max-w-[240px] object-cover"
                                // NOTE: cannot use /thumbnail because studio logos are often transparent => png
                                src={
                                  scene.studio.thumbnail?._id &&
                                  `/api/media/image/${scene.studio.thumbnail?._id}?password=xxx`
                                }
                                alt={`${scene.studio.name}`}
                              />
                            </LocaleLink>
                          </div>
                        ) : (
                          <Text className="text-sm">
                            <LocaleLink className="underline" to={`/studio/${scene.studio._id}`}>
                              {scene.studio.name}
                            </LocaleLink>
                          </Text>
                        )}
                      </CardSection>
                    )}
                    {scene.releaseDate && (
                      <CardSection title={t("releaseDate")}>
                        <Text className="text-sm">
                          {new Date(scene.releaseDate).toLocaleDateString()} (
                          <i className="opacity-60">{timeAgo(scene.releaseDate)})</i>
                        </Text>
                      </CardSection>
                    )}
                    {scene.description && (
                      <CardSection title={t("description")}>
                        <Description>{scene.description}</Description>
                      </CardSection>
                    )}
                    <CardSection title={t("rating")}>
                      <Rating onChange={changeRating} value={scene.rating} />
                    </CardSection>
                    {hasRatedAllMarkers && (
                      <CardSection title={t("avgRating")}>
                        <Text className="text-xs mb-2">
                          Calculated using marker ratings. All markers need to be rated.
                        </Text>
                        <Rating readonly value={Math.floor(averageRatingByMarkers)} />
                      </CardSection>
                    )}
                    <CardSection title={t("heading.labels")}>
                      <LabelGroup
                        limit={999}
                        value={labels}
                        onChange={setLabels}
                        onDelete={deleteLabel}
                        onEdit={async () => {
                          await updateLabels(
                            scene._id,
                            labels.map((l) => l._id)
                          );
                        }}
                      />
                    </CardSection>
                    <CardSection title={t("heading.previewGrid")}>
                      {gridLoader && <SkeletonCard color="bg-gray-300 dark:!bg-gray-700" />}
                      {showGrid && scene.fileExists ? (
                        <div className="flex flex-col gap-2">
                          <a
                            className="transition-all hover:brightness-75"
                            target="_blank"
                            href={gridUrl}
                          >
                            <img
                              className="bg-gray-300 dark:bg-gray-700 rounded-xl"
                              src={gridUrl}
                              width="100%"
                            />
                          </a>
                        </div>
                      ) : (
                        <>
                          {!scene.fileExists && <Description>Scene has no source file</Description>}
                        </>
                      )}
                    </CardSection>
                  </AutoLayout>
                  <AutoLayout>
                    <CardSection title={t("videoDuration")}>
                      <Text className="text-sm">{formatDuration(scene.meta.duration)}</Text>
                    </CardSection>
                    <CardSection title={t("addedToLibrary")}>
                      <Text className="text-sm">{new Date(scene.addedOn).toLocaleString()}</Text>
                    </CardSection>
                    <CardSection
                      title={
                        <div className="flex items-center gap-1">
                          {t("path")}{" "}
                          {!!scene.path && !scene.fileExists && (
                            <span className="text-red-600 dark:text-red-400 font-semibold">
                              [MISSING]
                            </span>
                          )}
                          <IconButton
                            Icon={CopyIcon}
                            onClick={() => {
                              if (scene.path) {
                                navigator.clipboard.writeText(scene.path).catch(() => { });
                              }
                            }}
                            size={16}
                          />
                        </div>
                      }
                    >
                      <Text className="text-sm">{scene.path ?? "-"}</Text>
                    </CardSection>
                    <CardSection title={t("fileSize")}>
                      <div title={`${scene.meta.size} bytes`}>
                        <Text className="text-sm">{prettyBytes(scene.meta.size)}</Text>
                      </div>
                    </CardSection>
                    <CardSection title={t("videoDimensions")}>
                      <Text className="text-sm">
                        {scene.meta.dimensions.width}x{scene.meta.dimensions.height}
                      </Text>
                    </CardSection>
                    <CardSection title={t("fps")}>
                      <Text className="text-sm">{scene.meta.fps.toFixed(2)}</Text>
                    </CardSection>
                    <CardSection title={t("bitrate")}>
                      <Text className="text-sm">
                        {((scene.meta.size / 1000 / scene.meta.duration) * 8).toFixed(0)} kBit/s
                      </Text>
                    </CardSection>
                    <CardSection title={t("heading.views")}>
                      <div className="flex items-center gap-1">
                        <Button loading={watchLoader} onClick={_watchScene}>
                          {watches.length} +
                        </Button>
                        <Button
                          disabled={!watches.length}
                          loading={watchLoader}
                          onClick={_unwatchScene}
                        >
                          -
                        </Button>
                      </div>
                      {!!watches.length && (
                        <Text style={{ marginTop: 8 }}>
                          Last watched: {new Date(watches[watches.length - 1]).toLocaleString()}
                        </Text>
                      )}
                    </CardSection>
                    <CardSection title={t("heading.actions")}>
                      <div className="flex flex-col gap-2">
                        <SceneThumbnailEditor sceneId={scene._id} />
                        <Button
                          disabled={!scene.fileExists}
                          loading={screenshotLoader}
                          onClick={handleScreenshotScene}
                        >
                          Use current frame as thumbnail
                        </Button>
                        <FFProbeButton disabled={!scene.fileExists} sceneId={scene._id} />
                        <Button
                          disabled={!scene.fileExists}
                          loading={pluginLoader}
                          onClick={handleRunScenePlugins}
                        >
                          Run plugins
                        </Button>
                        <Button
                          disabled={!scene.thumbnail}
                          className="bg-red-400 dark:bg-red-700 hover:bg-red-600 hover:dark:bg-red-900"
                          onClick={async () => {
                            await setSceneThumbnail(scene._id, null);
                            revalidate();
                          }}
                        >
                          Delete thumbnail
                        </Button>
                      </div>
                    </CardSection>
                  </AutoLayout>
                </div>
              </Card>

              <Card>
                <div className="flex items-center gap-3 justify-between">
                  <CardTitle>{t("customData")}</CardTitle>
                  <Button onClick={() => customFieldFormWindow.open()}>Edit custom data</Button>
                </div>
                <AutoLayout>
                  <Description>
                    <CustomFieldGrid items={scene.resolvedCustomFields} />
                  </Description>
                </AutoLayout>

                <Window
                  title="Edit custom data"
                  isOpen={customFieldFormWindow.isOpen}
                  onClose={customFieldFormWindow.close}
                  actions={
                    <>
                      <Button
                        onClick={async () => {
                          await updateCustomFields(scene._id, customFields);
                          customFieldFormWindow.close();
                          revalidate();
                        }}
                      >
                        Save
                      </Button>
                      <Button secondary onClick={customFieldFormWindow.close}>
                        Close
                      </Button>
                    </>
                  }
                >
                  <CustomFieldForm
                    value={customFields}
                    onChange={setCustomFields}
                    fields={scene.availableFields}
                  />
                </Window>
              </Card>

              <Tabs
                states={["actors", "markers", "movies", "images", "similar"] as TabState[]}
                value={activeTab}
                onChange={setActiveTab}
                formatTitle={(value) => {
                  return {
                    actors: t("heading.actors"),
                    movies: t("heading.movies"),
                    images: t("heading.images"),
                    similar: t("heading.similarScenes"),
                    markers: t("heading.markers"),
                  }[value];
                }}
              />
              {activeTab === "images" && <SceneDetailsImageList sceneId={scene._id} />}
              {activeTab === "actors" && (
                <>
                  <ListWrapper loading={false} noResults={scene.actors.length === 0}>
                    {scene.actors.map((actor) => (
                      <ActorCard
                        scene={scene}
                        onFav={revalidate}
                        onBookmark={revalidate}
                        onRate={revalidate}
                        key={actor._id}
                        actor={actor}
                      />
                    ))}
                  </ListWrapper>
                </>
              )}
              {activeTab === "movies" && (
                <ListWrapper loading={false} noResults={scene.movies.length === 0}>
                  {scene.movies.map((movie) => (
                    <MovieCard
                      onFav={revalidate}
                      onBookmark={revalidate}
                      key={movie._id}
                      movie={movie}
                    />
                  ))}
                </ListWrapper>
              )}
              {activeTab === "markers" && (
                <ListWrapper loading={false} noResults={scene.markers.length === 0}>
                  <MarkerList
                    markers={sortedMarkers}
                    onDelete={revalidate}
                    //  onEdit={revalidate}
                    onFav={revalidate}
                    onBookmark={revalidate}
                    onRate={revalidate}
                    onClick={(marker) => {
                      startPlayback(marker.time);
                      window.scrollTo({
                        left: 0,
                        top: 0,
                        behavior: "smooth",
                      });
                    }}
                  />
                </ListWrapper>
              )}
              {activeTab === "similar" && <SimilarScenesList sceneId={scene._id} />}
            </AutoLayout>
          </div>
        </div>
      </AutoLayout>
    </PageWrapper>
  );
}
