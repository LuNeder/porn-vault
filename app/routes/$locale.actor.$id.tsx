import { LoaderArgs, redirect } from "@remix-run/node";
import { useLoaderData, useRevalidator } from "@remix-run/react";
import DeleteIcon from "mdi-react/DeleteIcon.js";
import { Fragment, useEffect, useState } from "react";
import { useTranslations } from "use-intl";

import Button from "~/components/Button";
import CustomFieldForm from "~/components/CustomFieldForm";
import CustomFieldGrid from "~/components/CustomFieldGrid";
import { SelectableLabel } from "~/components/LabelDropdownChoice";
import Text from "~/components/Text";
import Window from "~/components/Window";
import { useLocaleNavigate } from "~/composables/use_navigate";
import { useWindow } from "~/composables/use_window";
import { removeActor, removeLabel, updateCustomFields , updateLabels } from "~/util/mutations/actor";

import ActorEditor from "../components/actor_details/ActorEditor";
import ActorProfile from "../components/actor_details/ActorProfile";
import ActorStats from "../components/actor_details/ActorStats";
import HeroImage from "../components/actor_details/HeroImage";
import ImageList from "../components/actor_details/ImageList";
import MovieList from "../components/actor_details/MovieList";
import SceneList from "../components/actor_details/SceneList";
import SimilarActorsList from "../components/actor_details/SimilarActorsList";
import AutoLayout from "../components/AutoLayout";
import Card from "../components/Card";
import CardSection from "../components/CardSection";
import CardTitle from "../components/CardTitle";
import ComplexGrid from "../components/ComplexGrid";
import Description from "../components/Description";
import IconButton from "../components/IconButton";
import LabelGroup from "../components/LabelGroup";
import PageWrapper from "../components/PageWrapper";
import Spacer from "../components/Spacer";
import Tabs from "../components/Tabs";
import { actorCardFragment } from "../fragments/actor";
import { IActor } from "../types/actor";
import { graphqlQuery } from "../util/gql";

export const loader = async ({ params }: LoaderArgs) => {
  const { getActorById } = await graphqlQuery<{
    getActorById: IActor;
  }>(q, {
    id: params.id,
  });

  if (!getActorById) {
    throw redirect("/actors");
  }

  return {
    actor: getActorById,
  };
};

const q = `
  query ($id: String!) {
    getActorById(id: $id) {
      ...ActorCard
      addedOn
      bornOn
      aliases
      averageRating
      percentWatched
      score
      numScenes
      diskSpace
      labels {
        _id
        name
        color
      }
      thumbnail {
        _id
        color
      }
      externalLinks {
        text
        url
      }
      altThumbnail {
        _id
      }
      watches
      hero {
        _id
        color
      }
      avatar {
        _id
        color
      }
      availableFields {
        _id
        name
        type
        unit
        values
      }
      resolvedCustomFields {
        field {
          _id
          name
          type
          unit
          values
        }
        value
      }
    }
  }
  ${actorCardFragment}
  `;

type TabState = "scenes" | "movies" | "images" | "similar";

export default function ActorPage() {
  const t = useTranslations();
  const { actor } = useLoaderData<typeof loader>();
  const navigate = useLocaleNavigate();
  const { revalidate } = useRevalidator();

  const [activeTab, setActiveTab] = useState<TabState>("scenes");

  const [labels, setLabels] = useState<SelectableLabel[]>(actor.labels);

  const customFieldFormWindow = useWindow();
  const [customFields, setCustomFields] = useState(() =>
    actor.resolvedCustomFields.reduce((acc, { field, value }) => {
      acc[field._id] = value;
      return acc;
    }, {} as Record<string, any>)
  );

  // NOTE: ugly
  useEffect(() => {
    setLabels(actor.labels);
    setCustomFields(
      actor.resolvedCustomFields.reduce((acc, { field, value }) => {
        acc[field._id] = value;
        return acc;
      }, {} as Record<string, any>)
    );
  }, [actor]);

  async function deleteLabel(id: string) {
    await removeLabel(actor._id, id);
    setLabels(labels.filter((label) => label._id !== id));
  }

  async function doDeleteActor() {
    if (window.confirm("Really delete?")) {
      await removeActor(actor._id);
      navigate("/actors");
    }
  }

  const leftCol = (
    <AutoLayout key={actor._id} gap={10}>
      <Card>
        {/* ACTION BAR */}
        <AutoLayout gap={10} layout="h">
          <Spacer />
          <ActorEditor onEdit={revalidate} actor={actor} />
          <IconButton
            Icon={DeleteIcon}
            onClick={doDeleteActor}
            className="text-black hover:text-gray-500 dark:text-white hover:dark:text-gray-400"
          />
        </AutoLayout>
        <ActorProfile
          actorId={actor._id}
          actorName={actor.name}
          age={actor.age}
          bornOn={actor.bornOn}
          avatarId={actor.avatar?._id}
          nationality={actor.nationality}
          rating={actor.rating}
          favorite={actor.favorite}
          bookmark={actor.bookmark}
          links={actor.externalLinks}
        />
      </Card>
    </AutoLayout>
  );

  const rightCol = (
    <AutoLayout key={actor._id}>
      <div className="flex flex-col gap-2">
        <div className="pl-2">
          <CardTitle>{t("heading.stats")}</CardTitle>
        </div>
        <ActorStats
          percentWatched={actor.percentWatched}
          numScenes={actor.numScenes}
          numWatches={actor.watches.length}
          averageRating={actor.averageRating}
          score={actor.score}
          diskSpace={actor.diskSpace}
        />
      </div>
      <div className="flex flex-col gap-2">
        <div className="pl-2">
          <CardTitle>{t("general")}</CardTitle>
        </div>
        <Card>
          <AutoLayout>
            <CardSection title={t("addedToLibrary")}>
              <Text className="text-sm">{new Date(actor.addedOn).toLocaleString()}</Text>
            </CardSection>
            {actor.bornOn && (
              <CardSection title={t("birthDate")}>
                <Text className="text-sm">{new Date(actor.bornOn).toLocaleDateString()}</Text>
              </CardSection>
            )}
            {!!actor.aliases.length && (
              <CardSection title={t("aliases")}>
                <Description>
                  <ul className="leading-7">
                    {actor.aliases
                      .sort((a, b) => (a.startsWith("regex:") ? 1 : a.localeCompare(b)))
                      .map((alias) => (
                        <Fragment key={alias}>
                          {alias.startsWith("regex:") ? (
                            <li key={alias}>
                              <span className="italic opacity-60">regex: </span>
                              {alias.replace("regex:", "")}
                            </li>
                          ) : (
                            <li key={alias}>{alias}</li>
                          )}
                        </Fragment>
                      ))}
                  </ul>
                </Description>
              </CardSection>
            )}
            {actor.description && (
              <CardSection title={t("description")}>
                <Description>{actor.description}</Description>
              </CardSection>
            )}
            <CardSection title="Labels">
              {/* TODO: should display actor.labels,
              but internally it should use it's own State
              same for Scene DetailsPage,
              also, add functionality to Lightbox */}
              <LabelGroup
                limit={999}
                value={labels}
                onChange={setLabels}
                onDelete={deleteLabel}
                onEdit={async () => {
                  await updateLabels(
                    actor._id,
                    labels.map((l) => l._id)
                  );
                }}
              />
            </CardSection>
          </AutoLayout>
        </Card>
        <Card>
          <div className="flex items-center gap-3 justify-between">
            <CardTitle>{t("customData")}</CardTitle>
            <Button onClick={() => customFieldFormWindow.open()}>Edit custom data</Button>
          </div>
          <AutoLayout>
            <Description>
              <CustomFieldGrid items={actor.resolvedCustomFields} />
            </Description>
          </AutoLayout>

          <Window
            title="Edit custom data"
            isOpen={customFieldFormWindow.isOpen}
            onClose={customFieldFormWindow.close}
            actions={
              <>
                <Button
                  onClick={async () => {
                    await updateCustomFields(actor._id, customFields);
                    customFieldFormWindow.close();
                    revalidate();
                  }}
                >
                  Save
                </Button>
                <Button secondary onClick={customFieldFormWindow.close}>
                  Close
                </Button>
              </>
            }
          >
            <CustomFieldForm
              value={customFields}
              onChange={setCustomFields}
              fields={actor.availableFields}
            />
          </Window>
        </Card>
      </div>
      <Tabs
        states={["scenes", "movies", "images", "similar"] as TabState[]}
        value={activeTab}
        onChange={setActiveTab}
        formatTitle={(value) => {
          return {
            scenes: t("heading.scenes"),
            movies: t("heading.movies"),
            images: t("heading.images"),
            similar: t("heading.similarActors"),
          }[value];
        }}
      />
      {activeTab === "scenes" && <SceneList actorId={actor._id} />}
      {activeTab === "movies" && <MovieList actorId={actor._id} />}
      {activeTab === "images" && <ImageList actorId={actor._id} />}
      {activeTab === "similar" && <SimilarActorsList actorId={actor._id} />}
    </AutoLayout>
  );

  return (
    <PageWrapper padless title={actor.name}>
      <HeroImage imageId={actor.hero?._id} />
      <div className="relative p-2">
        <ComplexGrid leftChildren={leftCol} rightChildren={rightCol} />
      </div>
    </PageWrapper>
  );
}
