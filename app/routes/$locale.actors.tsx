import FlagIcon from "mdi-react/FlagIcon.js";
import FlagOutlineIcon from "mdi-react/FlagOutlineIcon.js";
import LabelIcon from "mdi-react/LabelIcon.js";
import LabelOutlineIcon from "mdi-react/LabelOutlineIcon.js";
import StarOutline from "mdi-react/StarBorderIcon.js";
import StarHalf from "mdi-react/StarHalfFullIcon.js";
import Star from "mdi-react/StarIcon.js";
import { useTranslations } from "use-intl";

import Head from "~/components/Head";

import ActorCard from "../components/ActorCard";
import ActorCreator from "../components/ActorCreator";
import AutoLayout from "../components/AutoLayout";
import BookmarkIconButton from "../components/BookmarkIconButton";
import { CountrySelector } from "../components/CountrySelector";
import FavoriteIconButton from "../components/FavoriteIconButton";
import Flex from "../components/Flex";
import IconButtonMenu from "../components/IconButtonMenu";
import LabelSelector from "../components/LabelSelector";
import ListWrapper from "../components/ListWrapper";
import PageWrapper from "../components/PageWrapper";
import PaginatedQuery from "../components/PaginatedQuery";
import Rating from "../components/Rating";
import SimpleSelect from "../components/SimpleSelect";
import SortDirectionButton, { SortDirection } from "../components/SortDirectionButton";
import TextInput from "../components/TextInput";
import { editActorList, fetchActors } from "../composables/use_actor_list";
import useLabelList from "../composables/use_label_list";
import { usePaginatedQuery } from "../composables/use_paginated_query";
import { IActor } from "../types/actor";
import { createLocalStorageQueryManager } from "../util/persistent_query";
import GridSizeSelector, { getGridSize } from "~/components/GridSizeSelector";
import StudioCreator from "~/components/StudioCreator";
import CardTitle from "~/components/CardTitle";

const LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

const storageKey = "actors_search_query";
const persistentQuery = createLocalStorageQueryManager(storageKey);

export const DEFAULT_QUERY = {
  query: "",
  nationality: "",
  favorite: false,
  bookmark: false,
  rating: 0,
  sortBy: "relevance",
  sortDir: "desc" as SortDirection,
  include: [] as string[],
  letter: "", // TODO: letter
};

export default function ActorListPage() {
  const t = useTranslations();

  const gridSize = getGridSize();

  const { labels: labelList, loading: labelLoader } = useLabelList();
  const hasNoLabels = !labelLoader && !labelList.length;

  const {
    query,
    setQuery,
    getPage,
    gotoPage,
    gotoStart,
    items: actors,
    error,
    loading,
    numItems,
    numPages,
    page,
    resetAll,
    setItems,
  } = usePaginatedQuery({
    defaultQuery: DEFAULT_QUERY,
    fetchFn: fetchActors,
    persistentQuery,
  });

  const highlight = query.query.trim().split(" ");

  function editActor(actorId: string, fn: (actor: IActor) => IActor) {
    setItems((prev) => editActorList(prev, actorId, fn));
  }

  return (
    <PageWrapper title="Actors">
      <Head title={t("foundActors", { numItems })} />
      <AutoLayout>
        <div className="flex gap-2 justify-between items-center">
          <CardTitle>{loading ? "Loading..." : t("foundActors", { numItems })}</CardTitle>
          <div className="flex gap-2 items-center">
            <ActorCreator onCreate={() => getPage(page, query)} />
            <GridSizeSelector />
          </div>
        </div>
        <PaginatedQuery
          loading={loading}
          errorMessage={error}
          resetAll={resetAll}
          refreshPage={() => getPage(page, query)}
          filters={
            <>
              <TextInput
                className="max-w-[120px]"
                onEnter={gotoStart}
                value={query.query}
                onChange={(text) =>
                  setQuery((prev) => ({
                    ...prev,
                    query: text,
                  }))
                }
                placeholder={t("action.findContent")}
              />
              <IconButtonMenu
                value={!!query.nationality}
                activeIcon={FlagIcon}
                inactiveIcon={FlagOutlineIcon}
              >
                <CountrySelector
                  value={query.nationality}
                  onChange={(nationality) => {
                    setQuery((prev) => ({ ...prev, nationality }));
                  }}
                />
              </IconButtonMenu>
              <FavoriteIconButton
                value={query.favorite}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    favorite: !prev.favorite,
                  }));
                }}
              />
              <BookmarkIconButton
                value={query.bookmark}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    bookmark: !prev.bookmark,
                  }));
                }}
              />
              <IconButtonMenu
                value={!!query.rating}
                activeIcon={query.rating === 10 ? Star : StarHalf}
                inactiveIcon={StarOutline}
              >
                <Rating
                  value={query.rating}
                  onChange={(rating) => {
                    setQuery((prev) => ({
                      ...prev,
                      rating,
                    }));
                  }}
                />
              </IconButtonMenu>
              <IconButtonMenu
                counter={query.include.length}
                value={!!query.include.length}
                activeIcon={LabelIcon}
                inactiveIcon={LabelOutlineIcon}
                isLoading={labelLoader}
                disabled={hasNoLabels}
              >
                <LabelSelector
                  multiple
                  value={query.include}
                  searchFn={(query, label) =>
                    label.name.toLowerCase().includes(query.toLowerCase()) ||
                    label.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  itemChoices={labelList}
                  onChange={(labels) => {
                    setQuery((prev) => ({
                      ...prev,
                      include: labels,
                    }));
                  }}
                />
              </IconButtonMenu>
              <>
                <SimpleSelect
                  items={[
                    {
                      text: t("relevance"),
                      value: "relevance",
                    },
                    {
                      text: t("addedToCollection"),
                      value: "addedOn",
                    },
                    {
                      text: t("lastViewed"),
                      value: "lastViewedOn",
                    },
                    {
                      text: t("age"),
                      value: "bornOn",
                    },
                    {
                      text: t("rating"),
                      value: "rating",
                    },
                    {
                      text: t("avgRating"),
                      value: "avgRating",
                    },
                    {
                      text: t("pvScore"),
                      value: "score",
                    },
                    {
                      text: t("numScenes"),
                      value: "numScenes",
                    },
                    {
                      text: t("numViews"),
                      value: "numViews",
                    },
                    {
                      text: t("bookmark"),
                      value: "bookmark",
                    },
                  ]}
                  value={query.sortBy}
                  onChange={(sortBy) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortBy,
                    }));
                  }}
                />
                <SortDirectionButton
                  value={query.sortDir}
                  onChange={(sortDir) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortDir,
                    }));
                  }}
                />
              </>
            </>
          }
          currentPage={page}
          numPages={numPages}
          setPage={gotoPage}
        >
          <ListWrapper size={gridSize} loading={loading} noResults={!numItems}>
            {actors.map((actor) => (
              <ActorCard
                highlight={highlight}
                onFav={(value) => {
                  editActor(actor._id, (actor) => {
                    actor.favorite = value;
                    return actor;
                  });
                }}
                onBookmark={(value) => {
                  editActor(actor._id, (actor) => {
                    actor.bookmark = !!value;
                    return actor;
                  });
                }}
                onRate={(rating) => {
                  editActor(actor._id, (actor) => {
                    actor.rating = rating;
                    return actor;
                  });
                }}
                key={actor._id}
                actor={actor}
              />
            ))}
          </ListWrapper>
        </PaginatedQuery>
      </AutoLayout>
    </PageWrapper>
  );
}
