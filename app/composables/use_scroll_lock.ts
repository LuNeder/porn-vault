import { DependencyList, useEffect } from "react";

export function useScrollLock(doLock: boolean, deps: DependencyList) {
  useEffect(() => {
    if (doLock) {
      document.querySelector("html")!.style.overflow = "hidden";
    } else {
      document.querySelector("html")!.style.overflow = "auto";
    }
  }, deps);
}
