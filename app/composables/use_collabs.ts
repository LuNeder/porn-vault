import { useState } from "react";

import { IActor } from "../types/actor";
import { fetchCollabs } from "../util/collabs";
import { useMount } from "./use_mounted";

export function useCollabs(actorId?: string) {
  const [collabs, setCollabs] = useState<IActor[]>([]);
  const [loading, setLoader] = useState(true);

  async function loadCollabs() {
    if (actorId) {
      try {
        setLoader(true);
        const collabs = await fetchCollabs(actorId);
        setCollabs(collabs);
      } catch (error) {
        console.error(error);
      } finally {
        setLoader(false);
      }
    }
  }

  useMount(() => {
    loadCollabs().catch(() => {});
  });

  return { loading, collabs };
}
