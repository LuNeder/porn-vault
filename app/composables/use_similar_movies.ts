import { useState } from "react";

import { movieCardFragment } from "../fragments/movie";
import { IMovie } from "../types/movie";
import { graphqlQuery } from "../util/gql";
import { useMount } from "./use_mounted";

export function useSimilarMovies(movieId: string) {
  const [movies, setMovies] = useState<IMovie[]>([]);
  const [loading, setLoading] = useState(false);

  async function fetchSimilarMovies() {
    const q = `
    query ($id: String!) {
      getMovieById(id: $id) {
        similar {
          ...MovieCard
        }
      }
    }
    ${movieCardFragment}
  `;

    const { getMovieById } = await graphqlQuery<{
      getMovieById: {
        similar: IMovie[];
      };
    }>(q, {
      id: movieId,
    });

    setMovies(getMovieById.similar);
  }

  useMount(() => {
    setLoading(true);
    fetchSimilarMovies()
      .catch((_) => {})
      .finally(() => {
        setLoading(false);
      });
  });

  return {
    movies,
    setMovies,
    loading,
  };
}
