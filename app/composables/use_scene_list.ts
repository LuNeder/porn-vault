import { sceneCardFragment } from "../fragments/scene";
import { IPaginationResult } from "../types/pagination";
import { IScene } from "../types/scene";
import { graphqlQuery } from "../util/gql";

// TODO: move out of composables

export function editSceneList(
  items: IScene[],
  sceneId: string,
  fn: (scene: IScene) => IScene
): IScene[] {
  const copy = [...items];
  const index = copy.findIndex((scene) => scene._id === sceneId);
  if (index > -1) {
    const scene = copy[index];
    copy.splice(index, 1, fn(scene));
  }
  return copy;
}

export async function fetchScenes(page = 0, query: any) {
  const q = `
  query($query: SceneSearchQuery!) {
    getScenes(query: $query) {
      items {
        ...SceneCard
      }
      numItems
      numPages
    }
  }
  ${sceneCardFragment}
`;

  const { getScenes } = await graphqlQuery<{
    getScenes: IPaginationResult<IScene>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getScenes;
}
