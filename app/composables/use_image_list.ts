import { useState } from "react";

import { imageCardFragment } from "../fragments/image";
import { IImage } from "../types/image";
import { IPaginationResult } from "../types/pagination";
import { graphqlQuery } from "../util/gql";

export function editImageList(
  items: IImage[],
  imageId: string,
  fn: (image: IImage) => IImage
): IImage[] {
  const copy = [...items];
  const index = copy.findIndex((image) => image._id === imageId);
  if (index > -1) {
    const image = copy[index];
    copy.splice(index, 1, fn(image));
  }
  return copy;
}

export function useImageList(initial: IPaginationResult<IImage>, query: any) {
  const [images, setImages] = useState<IImage[]>(initial?.items || []);
  const [loading, setLoader] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [numItems, setNumItems] = useState(initial?.numItems ?? -1);
  const [numPages, setNumPages] = useState(initial?.numPages ?? -1);

  async function _fetchImages(page = 0) {
    try {
      setLoader(true);
      setError(null);
      const result = await fetchImages(page, query);
      setImages(result.items);
      setNumItems(result.numItems);
      setNumPages(result.numPages);
    } catch (fetchError: any) {
      setError(fetchError.message);
    }
    setLoader(false);
  }

  return {
    images,
    prependImages: (add: IImage[]) => {
      setImages((prev) => [...add, ...prev]);
      setNumItems((prev) => prev + add.length);
    },

    loading,
    error,
    numItems,
    numPages,
    fetchImages: _fetchImages,
  };
}

export async function fetchImages(page = 0, query: any) {
  const q = `
  query($query: ImageSearchQuery!) {
    getImages(query: $query) {
      items {
        ...ImageCard
      }
      numItems
      numPages
    }
  }
  ${imageCardFragment}
`;

  const { getImages } = await graphqlQuery<{
    getImages: IPaginationResult<IImage>;
  }>(q, {
    query: {
      query: "",
      page,
      sortBy: "addedOn",
      sortDir: "desc",
      ...query,
    },
  });

  return getImages;
}
