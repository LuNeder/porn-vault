import { useNavigate } from "@remix-run/react";
import { useLocale } from "use-intl";

export function useLocaleNavigate() {
  const navigate = useNavigate();
  const locale = useLocale();

  return (to: string) => {
    navigate(`/${locale}${to}`);
  };
}
