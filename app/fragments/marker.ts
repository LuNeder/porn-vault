export const markerPageFragment = `
fragment MarkerPage on Marker {
  _id
  name
  time
  rating
  favorite
  bookmark
  labels {
    _id
    name
    color
  }
  thumbnail {
    _id
    name
  }
  actors {
    _id
    name
  }
}
`;

export const markerCardFragment = `
fragment MarkerCard on Marker {
  _id
  name
  favorite
  bookmark
  rating
  labels {
    _id
    name
    color
  }
  actors {
    _id
    name
  }
  scene {
    _id
    name
  }
  time
  thumbnail {
    _id
  }
}
`;
