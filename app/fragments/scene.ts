import { actorCardFragment } from "./actor";
import { markerCardFragment } from "./marker";
import { movieCardFragment } from "./movie";

export const scenePageFragment = `
fragment ScenePage on Scene {
  _id
  name
  addedOn
  favorite
  bookmark
  rating
  description
  releaseDate
  fileExists
  labels {
    _id
    name
    color
  }
  thumbnail {
    _id
    color
  }
  meta {
    duration
    fps
    size
    bitrate
    dimensions {
      width
      height
    }
  }
  actors {
    ...ActorCard
  }
  movies {
    ...MovieCard
  }
  studio {
    _id
    name
    thumbnail {
      _id
    }
  }
  path
  watches
  markers {
    ...MarkerCard
  }
  availableFields {
    _id
    name
    type
    unit
    values
  }
  resolvedCustomFields {
    field {
      _id
      name
      type
      unit
    }
    value
  }
}
${actorCardFragment}
${movieCardFragment}
${markerCardFragment}
`;

export const sceneCardFragment = `
fragment SceneCard on Scene {
  _id
  name
  releaseDate
  favorite
  bookmark
  rating
  thumbnail {
    _id
    color
  }
  labels {
    _id
    name
    color
  }
  actors {
    _id
    name
  }
  studio {
    _id
    name
  }
  meta {
    duration
    size
    dimensions {
      width
      height
    }
  }
  watches
  availableFields {
    _id
    name
    type
    unit
  }
  customFields
}
`;
