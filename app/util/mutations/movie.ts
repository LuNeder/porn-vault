import { graphqlQuery } from "../gql";

export async function runMoviePlugins(movieId: string): Promise<void> {
  const mutation = `
mutation($id: String!) {
  runMoviePlugins(id: $id) {
    _id
  }
}
`;

  await graphqlQuery(mutation, {
    id: movieId,
  });
}

export async function removeMovie(id: string) {
  const query = `
mutation ($ids: [String!]!) {
  removeMovies(ids: $ids)
}`;

  await graphqlQuery(query, {
    ids: [id],
  });
}

export async function setMovieSpine(movieId: string, imageId: string) {
  const mutation = `
mutation ($ids: [String!]!, $opts: MovieUpdateOpts!) {
  updateMovies(ids: $ids, opts: $opts) {
    spineCover {
      _id
      color
    }
  }
}`;

  await graphqlQuery(mutation, {
    ids: [movieId],
    opts: {
      spineCover: imageId,
    },
  });
}

export async function setMovieBackCover(movieId: string, imageId: string) {
  const mutation = `
mutation ($ids: [String!]!, $opts: MovieUpdateOpts!) {
  updateMovies(ids: $ids, opts: $opts) {
    backCover {
      _id
      color
    }
  }
}`;

  await graphqlQuery(mutation, {
    ids: [movieId],
    opts: {
      backCover: imageId,
    },
  });
}

export async function setMovieFrontCover(movieId: string, imageId: string) {
  const mutation = `
mutation ($ids: [String!]!, $opts: MovieUpdateOpts!) {
  updateMovies(ids: $ids, opts: $opts) {
    frontCover {
      _id
      color
    }
  }
}`;

  await graphqlQuery(mutation, {
    ids: [movieId],
    opts: {
      frontCover: imageId,
    },
  });
}

export async function favoriteMovie(movieId: string, value: boolean): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: MovieUpdateOpts!) {
  updateMovies(ids: $ids, opts: $opts) {
    favorite
  }
}`;

  await graphqlQuery(mutation, {
    ids: [movieId],
    opts: {
      favorite: value,
    },
  });
}

export async function bookmarkMovie(movieId: string, value: Date | null): Promise<void> {
  const mutation = `
mutation($ids: [String!]!, $opts: MovieUpdateOpts!) {
  updateMovies(ids: $ids, opts: $opts) {
    bookmark
  }
}`;

  await graphqlQuery(mutation, {
    ids: [movieId],
    opts: {
      bookmark: value && value.valueOf(),
    },
  });
}
