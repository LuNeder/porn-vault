import { IActor } from "../types/actor";
import { graphqlQuery } from "./gql";

export async function fetchCollabs(actorId: string): Promise<IActor[]> {
  const data = await graphqlQuery<{
    getActorById: {
      collabs: IActor[];
    };
  }>(
    `
  query ($id: String!) {
    getActorById(id: $id) {
      collabs {
        _id
        name
        aliases
        avatar {
          _id
          color
        }
      }
    }
  }
  `,
    {
      id: actorId,
    }
  );
  return data.getActorById.collabs;
}
