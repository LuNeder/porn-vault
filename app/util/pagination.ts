function uniqueInOrder<T>(arr: T[]) {
  return arr.filter((x, i) => x !== arr[i + 1]);
}

export function createPaginationArray(
  current: number,
  total: number,
  buffer: number
): (number | "...")[] {
  if (total === 1) {
    return [1];
  }

  const center: number[] = [];
  const currentOneIndexed = current + 1;

  if (buffer) {
    for (let i = currentOneIndexed - buffer; i <= currentOneIndexed + buffer; i++) {
      if (i >= 1 && i <= total) {
        center.push(i);
      }
    }
  } else {
    center.push(currentOneIndexed);
  }

  const paddedCenter: (number | "...")[] = [...center];

  if (center.at(0)! > 2) {
    paddedCenter.unshift("...");
  }
  if (center.at(-1) !== total) {
    paddedCenter.push("...");
  }

  const arr = uniqueInOrder([1, ...paddedCenter, total]);

  return arr;
}
