import { useState } from "react";
import { useTranslations } from "use-intl";

import { useWindow } from "../composables/use_window";
import { graphqlQuery } from "../util/gql";
import Button from "./Button";
import CountryDropdownChoice, { SimpleCountry } from "./CountryDropdownChoice";
import InputError from "./InputError";
import { SelectableLabel } from "./LabelDropdownChoice";
import LabelGroup from "./LabelGroup";
import Subheading from "./Subheading";
import Textarea from "./Textarea";
import TextInput from "./TextInput";
import Window from "./Window";

async function createActor(
  name: string,
  aliases: string[],
  labels: string[],
  nationality: string | null
) {
  const query = `
  mutation ($name: String!, $aliases: [String!]!, $labels: [String!]!, $nationality: String) {
    addActor(name: $name, aliases: $aliases, labels: $labels, nationality: $nationality) {
      _id
    }
  }
        `;

  return await graphqlQuery(query, {
    name,
    aliases,
    labels,
    nationality,
  });
}

type Props = {
  onCreate: () => void;
};

export default function ActorCreator({ onCreate }: Props) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [error, setError] = useState<string | undefined>();
  const [name, setName] = useState("");
  const [aliasInput, setAliasInput] = useState("");
  const [selectedLabels, setSelectedLabels] = useState<SelectableLabel[]>([]);
  const [nationality, setNationality] = useState<
    SimpleCountry | null | undefined
  >(null);

  const [loading, setLoader] = useState(false);

  function reset() {
    setName("");
    setAliasInput("");
    setSelectedLabels([]);
    setNationality(null);
    setError(undefined);
  }

  function doClose() {
    close();
  }

  return (
    <>
      <Button onClick={open}>+ {t("action.addActor")}</Button>
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={t("action.addActor")}
        actions={
          <>
            <Button
              disabled={!name.length}
              loading={loading}
              onClick={async () => {
                try {
                  setLoader(true);
                  await createActor(
                    name,
                    aliasInput
                      .split("\n")
                      .map((x) => x.trim())
                      .filter(Boolean),
                    selectedLabels.map(({ _id }) => _id),
                    nationality?.alpha2 ?? null
                  );

                  onCreate();
                  reset();
                  doClose();
                } catch (error) {
                  console.error(error);
                  if (error instanceof Error) {
                    setError(error.message);
                  } else {
                    setError("An error occurred");
                  }
                }

                setLoader(false);
              }}
            >
              Create
            </Button>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <div>
          <Subheading>Actor name *</Subheading>
          <TextInput
            className="w-full"
            value={name}
            onChange={setName}
            placeholder="Enter an actor name"
          />
        </div>
        <div className="max-w-[250px]">
          <Subheading>Labels</Subheading>
          <LabelGroup
            limit={999}
            value={selectedLabels}
            onChange={setSelectedLabels}
            onDelete={(id) => {
              setSelectedLabels((prev) => prev.filter((x) => x._id !== id));
            }}
          />
        </div>
        <div>
          <Subheading>Nationality</Subheading>
          <CountryDropdownChoice
            value={nationality}
            onChange={setNationality}
          />
        </div>
        <div>
          <Subheading>Aliases</Subheading>
          <Textarea
            className="w-full"
            value={aliasInput}
            onChange={setAliasInput}
            placeholder="1 per line"
          />
        </div>
        {error && <InputError message={error} />}
      </Window>
    </>
  );
}
