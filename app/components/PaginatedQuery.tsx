import { ReactNode, useEffect, useState } from "react";
import { useTranslations } from "use-intl";

import AutoLayout from "./AutoLayout";
import Button from "./Button";
import CardTitle from "./CardTitle";
import Flex from "./Flex";
import Pagination from "./Pagination";
import Spacer from "./Spacer";
import TextInput from "./TextInput";

type Props<T, Q extends Record<string, any>> = {
  currentPage: number;
  numPages: number;
  setPage: (page: number) => void;
  children: ReactNode;
  overrideQuery?: Partial<Q>;
  filters?: ReactNode;
  refreshPage: () => void;
  resetAll: () => void;
  errorMessage: string | null;
  loading: boolean;
};

export default function PaginatedQuery<T, Q extends Record<string, any>>({
  currentPage,
  numPages,
  children,
  setPage,
  filters,
  refreshPage,
  resetAll,
  errorMessage,
  loading,
}: Props<T, Q>): JSX.Element {
  const t = useTranslations();

  const [pageInput, _setPageInput] = useState(currentPage);

  function setPageInput(input: string) {
    const parsed = parseInt(input) || 0;
    const newPage = Math.max(0, Math.min(parsed - 1, numPages - 1));
    _setPageInput(newPage);
  }

  useEffect(() => {
    _setPageInput(currentPage);
  }, [currentPage]);

  return (
    <AutoLayout>
      {/* TODO: remove */}
      <Flex layout="h" className="gap-2">
        {!loading && numPages > 1 && (
          <>
            <Spacer />
            <Pagination numPages={numPages} current={currentPage} onChange={setPage} />
            <TextInput
              onEnter={() => {
                if (pageInput !== currentPage) {
                  setPage(pageInput);
                } else {
                  refreshPage();
                }
              }}
              value={String(pageInput + 1)}
              onChange={setPageInput}
              placeholder="Jump"
              className="max-w-[60px]"
            />
          </>
        )}
      </Flex>
      <Flex wrap layout="h" className="gap-2">
        {filters}
        <Spacer />
        <Button secondary onClick={resetAll}>
          {t("action.reset")}
        </Button>
        <Button
          onClick={() => {
            if (pageInput !== currentPage) {
              setPage(pageInput);
            } else {
              refreshPage();
            }
          }}
        >
          {t("action.refresh")}
        </Button>
      </Flex>
      {errorMessage ? (
        <div className="text-center text-red-700 dark:text-red-300">{errorMessage}</div>
      ) : (
        <div>{children}</div>
      )}
      {!loading && numPages > 1 && (
        <Flex layout="h" className="justify-center gap-2">
          <Pagination numPages={numPages} current={currentPage} onChange={setPage} />
          <TextInput
            onEnter={() => {
              if (pageInput !== currentPage) {
                setPage(pageInput);
              } else {
                refreshPage();
              }
            }}
            value={String(pageInput + 1)}
            onChange={setPageInput}
            placeholder="Jump"
            className="max-w-[60px]"
          />
        </Flex>
      )}
    </AutoLayout>
  );
}
