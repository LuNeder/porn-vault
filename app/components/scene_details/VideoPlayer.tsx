import FullscreenIcon from "mdi-react/FullscreenIcon.js";
import React, { useEffect, useRef, useState } from "react";
import { useHotkeys } from "react-hotkeys-hook";

import { useMount } from "~/composables/use_mounted";

import { useSafeMode } from "../../composables/use_safe_mode";
import { useVideoControls } from "../../composables/use_video_control";
import { IScene } from "../../types/scene";
import { formatDuration } from "../../util/string";
import IconButton from "../IconButton";
import Loader from "../Loader";
import Marker from "../Marker";
import PlayPauseToggleButton from "../player/PlayPauseToggleButton";
import ScenePreview from "../ScenePreview";
import Spacer from "../Spacer";

const VIDEO_EL_ID = "video-player";
const SKIP_SECONDS = 5;

type Props = {
  src: string;
  poster?: string;
  markers: { name: string; time: number; thumbnail: string }[];
  duration: number;
  startAtPosition?: number;
  scene: IScene;
};

export default function VideoPlayer({
  src,
  poster,
  markers,
  duration,
  startAtPosition,
  scene,
}: Props) {
  const { blur: safeModeBlur } = useSafeMode();

  const didMountRef = useRef(false);
  const [loading, setLoading] = useState(true);
  const [fullscreen, setFullscreen] = useState(false);
  const [hover, setHover] = useState(false);
  const [hoverSeekBar, setHoverSeekBar] = useState(false);
  const [mousePosition, setMousePosition] = useState<{
    x: number;
    percent: number;
  }>({
    x: 0,
    percent: 0,
  });
  const [progress, setProgress] = useState(0);
  const [bufferRanges, setBufferRanges] = useState<{ start: number; end: number }[]>([]);
  const {
    setCurrentTime,
    setNewPlaybackTime,
    paused,
    startPlayback,
    newPlaybackTime,
    setScene,
    setPaused,
    reset,
    togglePlayback,
  } = useVideoControls();
  const videoEl = useRef<HTMLVideoElement | null>(null);

  // play/pause handling from VideoContext
  useEffect(() => {
    if (fullscreen) {
      return;
    }
    // avoid autoplay
    if (!didMountRef.current) {
      didMountRef.current = true;
      // unless we want to immediately start at a specific position (markers)
      if (!startAtPosition) {
        return;
      }
    }

    if (!videoEl.current) {
      return;
    }

    if (paused) {
      videoEl.current.pause();
    } else {
      videoEl.current
        .play()
        .catch((error) => {
          console.error(error);
        })
        .finally(() => {
          setScene(scene);
        });
    }
  }, [paused]);

  // reset player state on unmount
  useMount(() => {
    return reset;
  });

  // handle starting the playback at a specific position
  useMount(() => {
    if (videoEl.current && startAtPosition) {
      startPlayback(startAtPosition);
    }
  });

  // handle triggers for skipping the current playhead
  useEffect(() => {
    if (!newPlaybackTime) {
      return;
    }

    const vid = videoEl.current!;
    vid.currentTime = newPlaybackTime;
  }, [newPlaybackTime]);

  // Play/pause handling from video element to update the context
  useMount(() => {
    const handler = (event: any) => {
      setPaused(event.type !== "play");
    };

    videoEl.current?.addEventListener("play", handler, false);
    videoEl.current?.addEventListener("pause", handler, false);

    return () => {
      videoEl.current?.removeEventListener("play", handler);
      videoEl.current?.removeEventListener("pause", handler);
    };
  });

  // Loading chunks
  useMount(() => {
    const handler = () => {
      if (!videoEl.current) {
        return;
      }

      const vid = videoEl.current;
      const total = vid.duration;

      setBufferRanges(
        [...new Array(vid.buffered.length)].map((_, i) => ({
          start: vid.buffered.start(i) / total,
          end: vid.buffered.end(i) / total,
        }))
      );
    };
    videoEl.current?.addEventListener("progress", handler, false);
    return () => videoEl.current?.removeEventListener("progress", handler);
  });

  // Waiting (load)
  useMount(() => {
    const handler = () => {
      setLoading(true);
    };
    videoEl.current?.addEventListener("waiting", handler, false);
    return () => videoEl.current?.removeEventListener("waiting", handler);
  });

  // Time update
  useMount(() => {
    const handler = () => {
      const vid = videoEl.current!;

      // this happens when playing a video then navigating away.
      // TODO: reset state?
      if (!vid) {
        return;
      }

      const buffered = vid.currentTime;
      const duration = vid.duration;

      setCurrentTime(vid.currentTime);
      setProgress(buffered / duration);
      setLoading(false);
    };

    videoEl.current?.addEventListener("timeupdate", handler, false);
    return () => videoEl.current?.removeEventListener("timeupdate", handler);
  });

  // Full screen
  useMount(() => {
    const handler = () => {
      setFullscreen(!!document.fullscreenElement);
    };
    document.addEventListener("fullscreenchange", handler, false);
    return () => document.removeEventListener("fullscreenchange", handler);
  });

  useHotkeys(
    ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      const total = videoEl.current.duration;

      const parsedKey = parseInt(ev.key);

      if (parsedKey >= 0 && parsedKey <= 9) {
        videoEl.current.currentTime = total * (0.1 * parsedKey);
      }
    },
    []
  );

  useHotkeys(
    "f",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      if (document.fullscreenElement) {
        document.exitFullscreen();
      } else {
        videoEl.current.requestFullscreen();
      }
    },
    [paused]
  );

  useHotkeys(
    "k",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      setPaused(true);
    },
    [paused]
  );

  useHotkeys(
    "space",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      ev.preventDefault();
      togglePlayback();
    },
    [paused]
  );

  useHotkeys(
    "ArrowUp",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      ev.preventDefault();
      videoEl.current.volume = Math.min(1, videoEl.current.volume + 0.1);
    },
    []
  );

  useHotkeys(
    "ArrowDown",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      ev.preventDefault();
      videoEl.current.volume = Math.max(0, videoEl.current.volume - 0.1);
    },
    []
  );

  useHotkeys(
    "ArrowLeft",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      ev.preventDefault();
      videoEl.current.currentTime -= SKIP_SECONDS;
    },
    []
  );

  useHotkeys(
    "ArrowRight",
    (ev) => {
      if (!videoEl.current) {
        return;
      }

      ev.preventDefault();
      videoEl.current.currentTime += SKIP_SECONDS;
    },
    []
  );

  useHotkeys(
    "m",
    () => {
      if (!videoEl.current) {
        return;
      }

      const muted = videoEl.current.muted;
      videoEl.current.muted = !muted;
    },
    []
  );

  return (
    <div
      className="flex justify-center overflow-hidden bg-black"
      onMouseMove={() => setHover(true)}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <div className="relative max-w-[1000px]">
        <div className="pointer-events-none flex absolute w-full h-full items-center justify-center">
          {loading && !paused && <Loader />}
        </div>
        <video
          id={VIDEO_EL_ID}
          controls={fullscreen}
          ref={videoEl}
          poster={poster}
          src={src}
          width="100%"
          height="100%"
          onClick={togglePlayback}
          style={{ filter: safeModeBlur }}
        />
        <div
          className="transition-all absolute w-full bottom-0"
          style={{
            opacity: +hover,
            background: "#000000ee",
          }}
        >
          <div
            onMouseMove={(event: React.MouseEvent<HTMLDivElement>) => {
              const rect = event.currentTarget.getBoundingClientRect();
              let x = event.clientX - rect.left;
              // Do not go "outside" the width of rectangle
              x = Math.min(rect.right - rect.left, x);
              x = Math.max(0, x);
              const percent = x / rect.width;
              setMousePosition({ x, percent });
            }}
            onMouseEnter={() => setHoverSeekBar(true)}
            onMouseLeave={() => setHoverSeekBar(false)}
            onClick={(ev) => {
              const clickTarget = ev.target as HTMLDivElement;
              const clickTargetWidth = clickTarget.clientWidth;
              const xCoordInClickTarget = ev.nativeEvent.offsetX;
              const percent = xCoordInClickTarget / clickTargetWidth;
              const vid = videoEl.current!;
              setNewPlaybackTime(vid.duration * percent);
            }}
            className="relative h-[8px] w-full cursor-pointer overflow-hidden bg-gray-600 transition-all duration-100"
          >
            {bufferRanges.map(({ start, end }) => (
              <div
                className="pointer-events-none absolute h-full bg-gray-400"
                suppressHydrationWarning
                key={start}
                style={{
                  left: `calc(100% * ${start})`,
                  width: `calc(100% * ${end - start})`,
                }}
              />
            ))}
            <div
              className="pointer-events-none absolute h-full bg-pink-500 transition-all duration-100"
              style={{ width: `calc(100% * ${progress})` }}
            />
          </div>
          {markers.map((marker) => (
            <Marker
              onClick={() => {
                const vid = videoEl.current!;
                vid.currentTime = vid.duration * marker.time;
              }}
              key={marker.time}
              {...marker}
            />
          ))}
          <ScenePreview
            duration={duration}
            show={hoverSeekBar}
            absolutePosition={mousePosition.x}
            percentagePosition={mousePosition.percent}
            thumbnail={`/api/media/preview/${scene._id}`}
          />
          <div className="flex items-center gap-2 px-3 py-1">
            <PlayPauseToggleButton />
            {/* TODO: */}
            {/*  <VolumeHighIcon className="text-white" size={28} /> */}
            <span className="text-sm font-semibold text-white">
              {formatDuration(duration * progress)} / {formatDuration(duration)}
            </span>
            <Spacer />
            <IconButton
              alwaysLight
              size={28}
              Icon={FullscreenIcon}
              onClick={() => {
                videoEl.current?.requestFullscreen().catch(() => { });
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
