import clsx from "clsx";
import StarOutline from "mdi-react/StarBorderIcon.js";
import StarHalf from "mdi-react/StarHalfFullIcon.js";
import Star from "mdi-react/StarIcon.js";

type RatingProps = {
  value: number;
  readonly?: boolean;
  onChange?: (x: number) => void;
};

export default function Rating({ value, readonly, onChange }: RatingProps) {
  const fav = value === 10;
  const _readonly = readonly ?? false;

  const className = clsx("transition-all duration-75 disabled:cursor-not-allowed", {
    "hover:scale-[1.2] cursor-pointer": !_readonly,
    "cursor-not-allowed": _readonly,
  });

  function onClick(ev: React.MouseEvent<any>, index: number) {
    if (_readonly) {
      return;
    }

    ev.stopPropagation();

    const clickTarget = ev.target as HTMLElement;
    const clickTargetWidth = clickTarget.getBoundingClientRect().width;
    const xCoordInClickTarget = ev.nativeEvent.offsetX;

    let computedValue;
    if (clickTargetWidth / 2 > xCoordInClickTarget) {
      // clicked left
      computedValue = index * 2 - 1;
    } else {
      // clicked right
      computedValue = index * 2;
    }

    if (value === computedValue) {
      onChange?.(0);
    } else {
      onChange?.(computedValue);
    }
  }

  function renderStar(index: number) {
    if (index * 2 <= (value || 0)) {
      return (
        <Star
          className={clsx(className, {
            "text-red-500": fav,
            "text-blue-500": !fav,
          })}
          onClick={(ev) => onClick(ev, index)}
          key={index}
        />
      );
    }

    if (value && value % 2 === 1 && index * 2 === value + 1) {
      return (
        <StarHalf
          className={clsx(className, "text-blue-500")}
          onClick={(ev) => onClick(ev, index)}
          key={index}
        />
      );
    }

    return (
      <StarOutline
        className={clsx(className, "text-gray-500 dark:text-gray-400")}
        onClick={(ev) => onClick(ev, index)}
        key={index}
      />
    );
  }

  return <div className="inline-flex">{[1, 2, 3, 4, 5].map(renderStar)}</div>;
}
