import { ReactNode } from "react";

import { DEFAULT_GRID_SIZE } from "~/util/layout";

type Props = {
  children: ReactNode;
  size?: number;
  gap?: number;
};

export default function ListContainer({ children, size, gap }: Props): JSX.Element {
  return (
    <div
      className="grid"
      style={{
        gridTemplateColumns: `repeat(auto-fill, minmax(${size || DEFAULT_GRID_SIZE}px, 1fr))`,
        gridGap: gap || 5,
      }}
    >
      {children}
    </div>
  );
}
