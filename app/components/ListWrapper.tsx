import { ReactNode } from "react";

import ContentWrapper from "./ContentWrapper";
import ListContainer from "./ListContainer";
import SkeletonGrid from "./SkeletonGrid";

type Props = {
  children?: ReactNode;
  loading: boolean;
  noResults: boolean;
  size?: number;
};

export default function ListWrapper({ children, loading, noResults, size }: Props) {
  return (
    <ContentWrapper loading={loading} noResults={noResults} loader={<SkeletonGrid size={size} />}>
      <ListContainer size={size}>{children}</ListContainer>
    </ContentWrapper>
  );
}
