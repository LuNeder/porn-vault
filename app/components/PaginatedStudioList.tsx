import { defu } from "defu";
import LabelIcon from "mdi-react/LabelIcon.js";
import LabelOutlineIcon from "mdi-react/LabelOutlineIcon.js";
import { useTranslations } from "use-intl";

import useLabelList from "../composables/use_label_list";
import { PersistentQueryManager, usePaginatedQuery } from "../composables/use_paginated_query";
import { editStudioList, fetchStudios } from "../composables/use_studio_list";
import { IStudio } from "../types/studio";
import AutoLayout from "./AutoLayout";
import BookmarkIconButton from "./BookmarkIconButton";
import FavoriteIconButton from "./FavoriteIconButton";
import Flex from "./Flex";
import IconButtonMenu from "./IconButtonMenu";
import LabelSelector from "./LabelSelector";
import ListWrapper from "./ListWrapper";
import PageWrapper from "./PageWrapper";
import PaginatedQuery from "./PaginatedQuery";
import SimpleSelect from "./SimpleSelect";
import SortDirectionButton, { SortDirection } from "./SortDirectionButton";
import StudioCard from "./StudioCard";
import StudioCreator from "./StudioCreator";
import TextInput from "./TextInput";
import GridSizeSelector, { getGridSize, } from "./GridSizeSelector";
import CardTitle from "./CardTitle";
import { useAtom } from "jotai";

export const DEFAULT_QUERY = {
  query: "",
  favorite: false,
  bookmark: false,
  sortBy: "relevance",
  sortDir: "desc" as SortDirection,
  include: [] as string[],
};

type Props = {
  persistentQuery?: PersistentQueryManager<typeof DEFAULT_QUERY>;
  mergeQuery: Partial<typeof DEFAULT_QUERY>;
  writeHead?: boolean;
  withCreator?: boolean;
  padless?: boolean;
};

export default function PaginatedStudioList(props: Props): JSX.Element {
  const t = useTranslations();

  const gridSize = getGridSize();

  const { labels: labelList, loading: labelLoader } = useLabelList();
  const hasNoLabels = !labelLoader && !labelList.length;

  const {
    query,
    setQuery,
    getPage,
    gotoPage,
    gotoStart,
    items: studios,
    error,
    loading,
    numItems,
    numPages,
    page,
    resetAll,
    setItems,
  } = usePaginatedQuery({
    defaultQuery: DEFAULT_QUERY,
    fetchFn: (page, query) => fetchStudios(page, defu(query, props.mergeQuery)),
    persistentQuery: props.persistentQuery,
  });

  const highlight = query.query.trim().split(" ");

  function editStudio(studioId: string, fn: (studio: IStudio) => IStudio) {
    setItems((prev) => editStudioList(prev, studioId, fn));
  }

  return (
    <PageWrapper
      padless={props.padless}
      title={props.writeHead ? t("foundStudios", { numItems }) : undefined}
    >
      <AutoLayout>
        <div className="flex gap-2 justify-between items-center">
          <CardTitle>{loading ? "Loading..." : t("foundStudios", { numItems })}</CardTitle>
          <div className="flex gap-2 items-center">
            {props.withCreator && (
              <Flex layout="h" className="gap-2">
                <StudioCreator onCreate={() => getPage(page, query)} />
              </Flex>
            )}
            <GridSizeSelector />
          </div>
        </div>
        <PaginatedQuery
          loading={loading}
          errorMessage={error}
          resetAll={resetAll}
          refreshPage={() => getPage(page, query)}
          filters={
            <>
              <TextInput
                className="max-w-[120px]"
                onEnter={gotoStart}
                value={query.query}
                onChange={(text) =>
                  setQuery((prev) => ({
                    ...prev,
                    query: text,
                  }))
                }
                placeholder={t("action.findContent")}
              />
              <FavoriteIconButton
                value={query.favorite}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    favorite: !prev.favorite,
                  }));
                }}
              />
              <BookmarkIconButton
                value={query.bookmark}
                onClick={() => {
                  setQuery((prev) => ({
                    ...prev,
                    bookmark: !prev.bookmark,
                  }));
                }}
              />
              <IconButtonMenu
                counter={query.include.length}
                value={!!query.include.length}
                activeIcon={LabelIcon}
                inactiveIcon={LabelOutlineIcon}
                isLoading={labelLoader}
                disabled={hasNoLabels}
              >
                <LabelSelector
                  multiple
                  value={query.include}
                  searchFn={(query, label) =>
                    label.name.toLowerCase().includes(query.toLowerCase()) ||
                    label.aliases.some((alias) => alias.toLowerCase().includes(query.toLowerCase()))
                  }
                  itemChoices={labelList}
                  onChange={(labels) => {
                    setQuery((prev) => ({
                      ...prev,
                      include: labels,
                    }));
                  }}
                />
              </IconButtonMenu>
              <>
                <SimpleSelect
                  items={[
                    {
                      text: t("relevance"),
                      value: "relevance",
                    },
                    {
                      text: t("addedToCollection"),
                      value: "addedOn",
                    },
                    {
                      text: t("avgRating"),
                      value: "averageRating",
                    },
                    {
                      text: t("numScenes"),
                      value: "numScenes",
                    },
                    {
                      text: t("bookmark"),
                      value: "bookmark",
                    },
                  ]}
                  value={query.sortBy}
                  onChange={(sortBy) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortBy: sortBy ?? "relevance",
                    }));
                  }}
                />
                <SortDirectionButton
                  value={query.sortDir}
                  onChange={(sortDir) => {
                    setQuery((prev) => ({
                      ...prev,
                      sortDir,
                    }));
                  }}
                />
              </>
            </>
          }
          currentPage={page}
          numPages={numPages}
          setPage={gotoPage}
        >
          <ListWrapper size={gridSize} loading={loading} noResults={!numItems} >
            {studios.map((studio) => (
              <StudioCard
                highlight={highlight}
                onFav={(value) => {
                  editStudio(studio._id, (studio) => {
                    studio.favorite = value;
                    return studio;
                  });
                }}
                onBookmark={(value) => {
                  editStudio(studio._id, (studio) => {
                    studio.bookmark = !!value;
                    return studio;
                  });
                }}
                key={studio._id}
                studio={studio}
              />
            ))}
          </ListWrapper>
        </PaginatedQuery>
      </AutoLayout>
    </PageWrapper>
  );
}
