import clsx from "clsx";
import { CSSProperties, ReactNode, useMemo } from "react";

import { useTheme } from "~/composables/use_dark_theme";

import { generateThumbnailPlaceholderColor } from "../util/color";

type Props = {
  src?: string | null;
  aspectRatio?: string;
  children?: ReactNode;
  objectFit?: "cover" | "contain";
  imgStyle?: CSSProperties;
  containerStyle?: CSSProperties;
  placeholder?: ReactNode;
  className?: string;
};

export default function ResponsiveImage({
  src,
  aspectRatio,
  children,
  objectFit,
  imgStyle,
  containerStyle,
  placeholder,
  className,
}: Props) {
  const { theme } = useTheme();
  const color = useMemo(() => generateThumbnailPlaceholderColor(theme === "dark"), [theme]);

  const inner = src ? (
    <div className="overflow-hidden"
    >
      <img
        loading="lazy"
        className={className}
        style={{ objectFit: objectFit || "cover", aspectRatio, ...imgStyle }}
        width="100%"
        src={src}
      />
    </div>
  ) : (
    <div
      className={clsx(className, "border-gray-300 dark:border-gray-800")}
      style={{ aspectRatio }}
    >
      <span className="absolute opacity-10 text-5xl left-[50%] top-[50%] translate-x-[-50%] translate-y-[-50%]">
        {placeholder || <>?</>}
      </span>
    </div>
  );

  return (
    <div
      className="relative"
      suppressHydrationWarning={true}
      style={{
        backgroundColor: color,
        ...containerStyle,
      }}
    >
      {inner}
      {children}
    </div>
  );
}
