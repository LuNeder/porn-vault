import clsx from "clsx";
import { CSSProperties } from "react";

type Props = {
  className?: string;
  style?: CSSProperties;
};

export default function Loader({ className, style }: Props) {
  return (
    <div
      style={style}
      className={clsx(
        "loader",
        className,
        "h-[16px] w-[16px] rounded-[50%] border-2 border-gray-200"
      )}
    />
  );
}
