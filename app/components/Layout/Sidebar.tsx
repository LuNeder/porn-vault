import { useLocation } from "@remix-run/react";
import clsx from "clsx";
import ActorIcon from "mdi-react/AccountBoxIcon.js";
import StudioIcon from "mdi-react/CameraAltIcon.js";
import SettingsIcon from "mdi-react/CogIcon.js";
import MovieIcon from "mdi-react/FilmstripBoxMultipleIcon.js";
import UnsafeModeIcon from "mdi-react/FireIcon.js";
import SafeModeIcon from "mdi-react/FlowerIcon.js";
import ViewHistoryIcon from "mdi-react/HistoryIcon.js";
import ImageIcon from "mdi-react/ImageIcon.js";
import LabelsIcon from "mdi-react/LabelIcon.js";
import CustomFieldsIcon from "mdi-react/TableCogIcon.js";
// import MarkerIcon from "mdi-react/PlaylistStarIcon.js";
import SceneIcon from "mdi-react/VideocamIcon.js";
import DarkThemeIcon from "mdi-react/WeatherNightIcon.js";
import LightThemeIcon from "mdi-react/WhiteBalanceSunnyIcon.js";
import { Fragment, ReactNode } from "react";
import { useLocale, useTranslations } from "use-intl";

import { useTheme } from "~/composables/use_dark_theme";
import { useSafeMode } from "~/composables/use_safe_mode";
import { useVersion } from "~/composables/use_version";

import Divider from "../Divider";
import Flag from "../Flag";
import Paper from "../Paper";
import Spacer from "../Spacer";
import SidebarLink from "./SidebarLink";

const links: (
  | { divider: true }
  | { divider: false; text: string; icon: ReactNode; url: string }
)[] = [
  {
    text: "heading.scenes",
    icon: <SceneIcon />,
    url: "/scenes",
    divider: false,
  },
  {
    text: "heading.actors",
    icon: <ActorIcon />,
    url: "/actors",
    divider: false,
  },
  {
    text: "heading.movies",
    icon: <MovieIcon />,
    url: "/movies",
    divider: false,
  },
  {
    text: "heading.studios",
    icon: <StudioIcon />,
    url: "/studios",
    divider: false,
  },
  {
    text: "heading.images",
    icon: <ImageIcon />,
    url: "/images",
    divider: false,
  },
  /*   {
    text: "heading.markers",
    icon: <MarkerIcon />,
    url: "/markers",
    divider: false,
  }, */
  { divider: true },
  {
    text: "heading.labels",
    icon: <LabelsIcon />,
    url: "/labels",
    divider: false,
  },
  {
    text: "heading.customFields",
    icon: <CustomFieldsIcon />,
    url: "/custom-fields",
    divider: false,
  },
  {
    text: "heading.viewHistory",
    icon: <ViewHistoryIcon />,
    url: "/view-history",
    divider: false,
  },
  {
    text: "heading.settings",
    icon: <SettingsIcon />,
    url: "/settings",
    divider: false,
  },
];

const languages: [string, string, string][] = [
  ["English", "US", "en"],
  ["Deutsch", "DE", "de"],
  /*  ["Francais", "FR", "fr"], */
];

type Props = {
  active: boolean;
  setSidebar: (x: boolean) => void;
};

export default function Sidebar({ active, setSidebar }: Props) {
  const t = useTranslations();
  const { enabled: safeMode, toggle: toggleSafeMode } = useSafeMode();
  const { version } = useVersion();
  const locale = useLocale();
  const location = useLocation();

  const { theme, toggle: toggleTheme } = useTheme();

  const sidebarContent = (
    <>
      <div className="flex grow flex-col gap-1 overflow-y-scroll">
        {links.map((link, i) => (
          <Fragment key={i}>
            {link.divider ? (
              <Divider />
            ) : (
              <SidebarLink onClick={() => setSidebar(false)} icon={link.icon} url={link.url}>
                {t(link.text, { numItems: 2 })}
              </SidebarLink>
            )}
          </Fragment>
        ))}
      </div>
      <Spacer />
      <div className="flex flex-col items-center gap-4">
        <div className="flex justify-center gap-2">
          {theme === "dark" ? (
            <DarkThemeIcon
              className="cursor-pointer text-gray-800 transition-colors hover:text-gray-600 dark:text-gray-400 hover:dark:text-gray-600"
              size={24}
              onClick={toggleTheme}
            />
          ) : (
            <LightThemeIcon
              className="cursor-pointer text-gray-800 transition-colors hover:text-gray-600 dark:text-gray-400 hover:dark:text-gray-600"
              size={24}
              onClick={toggleTheme}
            />
          )}
          {safeMode ? (
            <SafeModeIcon
              className="cursor-pointer text-gray-800 transition-colors hover:text-gray-600 dark:text-gray-400 hover:dark:text-gray-600"
              size={24}
              onClick={toggleSafeMode}
            />
          ) : (
            <UnsafeModeIcon
              className="cursor-pointer text-gray-800 transition-colors hover:text-gray-600 dark:text-gray-400 hover:dark:text-gray-600"
              size={24}
              onClick={toggleSafeMode}
            />
          )}
        </div>
        <div className="flex justify-center">
          <div className="flex justify-center gap-2">
            {languages.map(([name, code, newLocale]) => (
              <a key={newLocale} href={location.pathname.replace(`/${locale}`, `/${newLocale}`)}>
                <Flag
                  className="cursor-pointer transition-all hover:opacity-70"
                  name={name}
                  code={code}
                  size={24}
                />
              </a>
            ))}
          </div>
        </div>
        <div className="text-center text-gray-800 dark:text-gray-400">v{version}</div>
      </div>
    </>
  );

  return (
    <>
      {active && (
        <div
          className="mobile-sidebar-darken fixed left-0 top-0 h-full w-full"
          onClick={() => setSidebar(false)}
        />
      )}
      <Paper
        className={clsx(
          { active },
          "mobile-sidebar fixed top-0 rounded-none bg-gray-100 p-2 dark:bg-gray-900"
        )}
      >
        <div className="flex flex-col h-full gap-2">{sidebarContent}</div>
      </Paper>
      <div className="sidebar h-full min-h-[100vh] bg-gray-100 dark:bg-gray-900">
        <div className="inner sticky flex flex-col px-1 py-3">{sidebarContent}</div>
      </div>
    </>
  );
}
