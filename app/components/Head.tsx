import { useEffect } from "react";

export default function Head({ title }: { title: string }) {
  useEffect(() => {
    document.title = title;
  }, [title]);

  return <></>;
}
