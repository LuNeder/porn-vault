import clsx from "clsx";
import prettyBytes from "pretty-bytes";
import { useTranslations } from "use-intl";

type Props = {
  collections: {
    name: string;
    size: number;
    count: number;
    path: string;
  }[];
};

export default function IzzyTable({ collections }: Props): JSX.Element {
  const t = useTranslations();

  return (
    <table className="w-full overflow-hidden border border-gray-200 dark:border-gray-800">
      <thead className="text-sm uppercase text-gray-800 dark:text-gray-200">
        <tr>
          <th className="py-2 px-3 text-left">{t("name")}</th>
          <th className="py-2 px-3 text-left">{t("path")}</th>
          <th className="py-2 px-3 text-right">{t("size")}</th>
          <th className="py-2 px-3 text-right">{t("documentCount")}</th>
        </tr>
      </thead>
      <tbody className="text-sm text-gray-700 dark:text-gray-400">
        {collections.map(({ name, size, count, path }, index) => (
          <tr
            key={name}
            className={clsx({
              "bg-gray-200 dark:bg-gray-800": index % 2 === 0,
            })}
          >
            <td className="py-2 px-3 text-left">{name}</td>
            <td className="py-2 px-3 text-left">{path}</td>
            <td className="py-2 px-3 text-right" title={`${size} bytes`}>
              {prettyBytes(size)}
            </td>
            <td className="py-2 px-3 text-right">{count}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
