import clsx from "clsx";
import { CSSProperties, ReactNode } from "react";

type Props = {
  children: ReactNode;
  style?: CSSProperties;
  className?: string;
};

export default function Text({ className, children, style }: Props) {
  return (
    <div className={clsx(className, "text-gray-600 dark:text-gray-400")} style={style}>
      {children}
    </div>
  );
}
