import clsx from "clsx";
import { CSSProperties, ReactNode } from "react";

import AutoLayout from "./AutoLayout";
import Paper from "./Paper";

type Props = {
  children: ReactNode;
  style?: CSSProperties;
  className?: string;
  padless?: boolean;
  flat?: boolean;
  innerClassName?: string;
  layout?: "h" | "v";
};

export default function Card(props: Props) {
  const { children, className, style, padless, flat, innerClassName } = props;

  const _padless = padless ?? false;

  return (
    <Paper
      bordered
      rounded
      flat={flat}
      className={clsx(className, {
        "p-2": !_padless,
        "shadow-sm": !flat,
      })}
      style={style}
    >
      <AutoLayout gap={5} className={innerClassName} layout={props.layout}>
        {children}
      </AutoLayout>
    </Paper>
  );
}
