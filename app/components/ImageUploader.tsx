import CloseIcon from "mdi-react/CloseIcon.js";
import { useState } from "react";
import { useTranslations } from "use-intl";

import useUpdateEffect from "../composables/use_update_effect";
import { useWindow } from "../composables/use_window";
import { IImage } from "../types/image";
import { uploadImage } from "../util/mutations/image";
import Button from "./Button";
import FileInput from "./FileInput";
import Window from "./Window";

function readImage(file: File): Promise<string> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = () => {
      if (reader.result) {
        resolve(reader.result.toString());
      } else {
        reject(new Error("File error"));
      }
    };
    reader.onerror = reject;
    reader.readAsDataURL(file);
  });
}

type Props = {
  onUpload: (img: IImage[]) => void;
  onDone?: () => void;
  scene?: string;
  actors?: string[];
  labels?: string[];
};

export default function ImageUploader({ actors, scene, onUpload, onDone }: Props) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();

  const [uploadItems, setUploadItems] = useState<{ file: File; b64: string; name: string }[]>([]);
  const [uploadQueue, setUploadQueue] = useState<{ file: File; b64: string; name: string }[]>([]);
  const [loading, setLoader] = useState(false);

  useUpdateEffect(() => {
    const item = uploadQueue[0];

    if (item) {
      handleUpload(item).catch(() => {});
    } else {
      setLoader(false);
      onDone?.();
    }
  }, [uploadQueue.length]);

  async function addFiles(files: File[]) {
    for (const file of files) {
      const b64 = await readImage(file);

      if (uploadItems.find((i) => i.b64 === b64)) {
        continue;
      }

      setUploadItems((prev) => [...prev, { file, b64, name: file.name }]);
    }

    const inputEl = document.getElementById("file-input") as HTMLInputElement | undefined;
    if (inputEl) {
      inputEl.value = "";
    }
  }

  async function handleUpload(image: { file: File; b64: string; name: string }) {
    setLoader(true);
    try {
      const img = await uploadImage(
        {
          blob: image.file,
          name: image.name,
        },
        actors ?? [],
        scene
      );
      setUploadQueue(([_, ...rest]) => rest);
      onUpload([img]);
    } catch (error) {
      setUploadQueue([]);
      setUploadItems((prev) => [...prev, ...uploadQueue]);
    }
  }

  function spliceImage(index: number): void {
    setUploadItems((prev) => {
      const copy = [...prev];
      copy.splice(index, 1);
      return copy;
    });
  }

  return (
    <>
      <Button onClick={open} style={{ marginRight: 10 }}>
        {t("action.upload")}
      </Button>
      <Window
        actions={
          <>
            <Button
              disabled={!uploadItems.length}
              loading={loading}
              onClick={() => {
                setUploadQueue(uploadItems);
                setUploadItems([]);
              }}
            >
              {t("action.upload")}
            </Button>
          </>
        }
        isOpen={isOpen}
        onClose={close}
        title={t("action.uploadImages")}
      >
        <div>{uploadItems.length} files selected</div>
        <div className="flex gap-2">
          <FileInput
            onChange={addFiles}
            multiple
            accept={[".png", ".jpg", ".jpeg", ".webp", ".gif"]}
          />
          <Button
            disabled={loading || !uploadItems.length}
            secondary
            onClick={() => setUploadItems([])}
          >
            {t("action.reset")}
          </Button>
        </div>
        <div className="grid sm:grid-cols-2 gap-1 overflow-y-scroll max-h-[50vh]">
          {/* NOTE: base64 as key is OK because of addFiles function, which checks for duplicates */}
          {uploadItems.map((item, i) => (
            <div key={item.b64} className="relative sm:max-h-[150px]">
              <img src={item.b64} width="100%" className="h-full object-contain overflow-hidden" />
              <div className="absolute right-[8px] top-[8px] p-1 cursor-pointer transition-all hover:bg-opacity-60 bg-black bg-opacity-40 rounded-[50%]">
                <CloseIcon onClick={() => spliceImage(i)} size={24} />
              </div>
            </div>
          ))}
        </div>
        {loading && <div>Uploading {uploadQueue.length} images</div>}
      </Window>
    </>
  );
}
