import SettingsIcon from "mdi-react/SettingsIcon.js";
import { ReactNode } from "react";
import useSWR from "swr";
import { useTranslations } from "use-intl";

import { graphqlQuery } from "../../util/gql";
import Loader from "../Loader";
import Text from "../Text";
import WidgetCard from "./WidgetCard";

async function getQueueStats() {
  const query = `
  { 
    getQueueInfo {    
      length 
      processing 
    }
  }`;

  const { getQueueInfo } = await graphqlQuery<{
    getQueueInfo: {
      length: number;
      processing: boolean;
    };
  }>(query, {});

  return getQueueInfo;
}

export default function ProcessingCard() {
  const t = useTranslations();

  const { data: stats } = useSWR("processingQueueStats", getQueueStats, {
    revalidateOnFocus: true,
    revalidateOnReconnect: true,
    revalidateOnMount: true,
    refreshInterval: 2_500,
  });

  const Inner = ({ children }: { children: ReactNode }) => (
    <WidgetCard icon={<SettingsIcon />} title={t("widgets.videoProcessingQueue")}>
      {children}
    </WidgetCard>
  );

  if (!stats) {
    <Inner>No data</Inner>;
  }

  return (
    <Inner>
      <div className="flex items-center gap-2">
        <div>
          <Text>{stats?.length ? <>Queued: {stats.length}</> : <>-</>}</Text>
        </div>
        {stats?.processing && <Loader />}
      </div>
    </Inner>
  );
}
