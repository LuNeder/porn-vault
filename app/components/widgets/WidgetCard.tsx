import type { ReactNode } from "react";

import AutoLayout from "../AutoLayout";
import Card from "../Card";

type Props = {
  children: ReactNode;
  icon: ReactNode;
  title: string;
};

export default function WidgetCard({ children, icon, title }: Props) {
  return (
    <Card>
      <AutoLayout>
        <div className="flex items-center gap-3">
          {icon} <span>{title}</span>
        </div>
        <AutoLayout gap={10}>{children}</AutoLayout>
      </AutoLayout>
    </Card>
  );
}
