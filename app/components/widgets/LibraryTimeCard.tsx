import TimeIcon from "mdi-react/TimelapseIcon.js";
import { ReactNode } from "react";
import useSWR from "swr";
import { useTranslations } from "use-intl";

import Text from "../Text";
import WidgetCard from "./WidgetCard";

async function getWatchStats() {
  const res = await fetch("/api/remaining-time");
  return res.json();
}

export default function LibraryTimeCard() {
  const t = useTranslations();

  const { data: stats } = useSWR("libraryTimeStats", getWatchStats, {
    revalidateOnFocus: true,
    revalidateOnReconnect: true,
    revalidateOnMount: true,
    refreshInterval: 300_000,
  });

  const Inner = ({ children }: { children: ReactNode }) => (
    <WidgetCard icon={<TimeIcon />} title={t("widgets.libraryTime")}>
      {children}
    </WidgetCard>
  );

  if (!stats) {
    return (
      <Inner>
        <Text>No data</Text>
      </Inner>
    );
  }

  return (
    <>
      <Inner>
        <Text>
          {t("widgets.viewsInDays", {
            numViews: stats.numViews,
            numDays: +stats.currentIntervalDays.toFixed(0),
          })}
        </Text>
        <Text>
          {t("widgets.percentWatched", {
            percent: `${(stats.viewedPercent * 100).toFixed(1)}%`,
          })}
        </Text>
        <Text>
          {t("widgets.contentLeft", {
            years: stats.remainingYears.toFixed(1),
          })}
        </Text>
        <Text>
          {t("widgets.runningOut", {
            date: new Date(stats.remainingTimestamp).toLocaleDateString(),
          })}
        </Text>
      </Inner>
    </>
  );
}
