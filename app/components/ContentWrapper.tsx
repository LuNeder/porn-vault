import { ReactNode } from "react";

type Props = {
  children?: ReactNode;
  loader: ReactNode;
  loading: boolean;
  noResults: boolean;
};

export default function ContentWrapper({ loading, loader, children, noResults }: Props) {
  if (loading) {
    return <>{loader}</>;
  }
  if (noResults) {
    return (
      <div className="text-center text-lg font-semibold text-black dark:text-white">No results</div>
    );
  }
  return <>{children}</>;
}
