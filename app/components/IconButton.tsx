import clsx from "clsx";
import { MdiReactIconComponentType } from "mdi-react";
import React from "react";

type Props = {
  Icon: MdiReactIconComponentType;
  onClick?: (ev: React.MouseEvent<SVGSVGElement, MouseEvent>) => void;
  className?: string;
  alwaysLight?: boolean;
  size?: number;
  disabled?: boolean;
};

export default function IconButton(props: Props): JSX.Element {
  const { className, Icon, onClick, alwaysLight, size } = props;

  const defaultClasses =
    alwaysLight ?? false
      ? "text-white hover:text-gray-400"
      : "text-black dark:text-white hover:text-gray-500 hover:dark:text-gray-400";

  return (
    <Icon
      size={size}
      onClick={onClick}
      className={clsx(
        {
          "text-gray-400 dark:text-gray-600 cursor-not-allowed": props.disabled,
          [`cursor-pointer hover:translate-y-[-1px] ${defaultClasses}`]: !props.disabled,
        },
        className,
        "text-xl transition-all"
      )}
    />
  );
}
