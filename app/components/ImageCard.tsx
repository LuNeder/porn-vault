import clsx from "clsx";
import { ReactNode } from "react";

import { useSafeMode } from "../composables/use_safe_mode";

type Props = {
  src: string;
  alt?: string;
  onClick?: (event: React.MouseEvent<HTMLImageElement, MouseEvent>) => void;
  children?: ReactNode;
  className?: string;
};

export default function ImageCard(props: Props) {
  const { className, children, alt, src, onClick } = props;

  const { blur: safeModeBlur } = useSafeMode();

  return (
    <>
      <div className={clsx("relative overflow-hidden rounded-lg transition-all", className)}>
        <img
          className="block hover:brightness-75 transition-all cursor-pointer"
          width="100%"
          src={src}
          alt={alt}
          onClick={onClick}
          style={{
            filter: safeModeBlur,
          }}
        />
        {children && (
          <div className="pointer-events-none absolute left-0 top-0 w-full h-full">{children}</div>
        )}
      </div>
    </>
  );
}
