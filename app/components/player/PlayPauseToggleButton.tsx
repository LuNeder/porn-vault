import PauseIcon from "mdi-react/PauseIcon.js";
import PlayIcon from "mdi-react/PlayIcon.js";

import { useVideoControls } from "../../composables/use_video_control";
import IconButton from "../IconButton";

export default function PlayPauseToggleButton() {
  const { paused, togglePlayback } = useVideoControls();

  const PlaybackButton = paused ? PlayIcon : PauseIcon;

  return (
    <IconButton alwaysLight Icon={PlaybackButton} onClick={togglePlayback} />
  );
}
