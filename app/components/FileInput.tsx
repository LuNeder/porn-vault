import { ReactNode, useId } from "react";
import { useTranslations } from "use-intl";

import Button from "./Button";

type Props = {
  onChange?: (files: File[]) => void;
  accept?: string[];
  multiple?: boolean;
  children?: ReactNode;
  loading?: boolean;
};

export default function FileInput({ children, onChange, accept, multiple, loading }: Props) {
  const t = useTranslations();
  const id = useId();

  return (
    <>
      <Button loading={loading} onClick={() => document.getElementById(id)?.click()}>
        {children || <>{t("action.select")}</>}
      </Button>
      <input
        className="hidden"
        id={id}
        multiple={multiple ?? false}
        type="file"
        accept={accept?.join(", ")}
        onChange={(ev) => {
          ev.target.files && onChange?.(Array.from(ev.target.files));
        }}
      />
    </>
  );
}
