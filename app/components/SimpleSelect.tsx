import clsx from "clsx";

type Props = {
  value: string;
  onChange: (value: string | null) => void;
  items: { value: string; text: string }[];
  className?: string;
};

export default function SimpleSelect({ className, value, onChange, items }: Props) {
  return (
    <select
      className={clsx(
        className,
        "rounded border-2 px-2 py-1 !outline-none transition-colors",
        "border-gray-300 bg-gray-50 text-black hover:bg-gray-200 focus:border-blue-500 focus:bg-gray-100",
        "dark:border-gray-700 dark:bg-gray-800 dark:text-white hover:dark:bg-gray-900 focus:dark:border-blue-800 focus:dark:bg-gray-900"
      )}
      value={value}
      onChange={(ev) => {
        onChange(ev.target.value || null);
      }}
    >
      <option key={"DEFAULT EMPTY VALUE"} value="">
        -
      </option>
      {items.map(({ text, value }) => (
        <option key={value} value={value}>
          {text}
        </option>
      ))}
    </select>
  );
}
