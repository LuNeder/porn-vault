import { useState } from "react";
import { useTranslations } from "use-intl";

import { useMount } from "~/composables/use_mounted";

import { useSafeMode } from "../composables/use_safe_mode";
import { useSettings } from "../composables/use_settings";
import { useWindow } from "../composables/use_window";
import { IImage } from "../types/image";
import { graphqlQuery } from "../util/gql";
import {
  setActorAltThumbnail,
  setActorAvatar,
  setActorHero,
  setActorThumbnail,
} from "../util/mutations/actor";
import { uploadImage } from "../util/mutations/image";
import { imageUrl } from "../util/thumbnail";
import Button from "./Button";
import { ImageCropper } from "./Cropper";
import FileInput from "./FileInput";
import Flex from "./Flex";
import Window from "./Window";

type imageTypes = "avatar" | "thumbnail" | "altThumbnail" | "hero";

type ActorImage = Pick<IImage, "_id" | "name">;

type ActorImages = {
  altThumbnail?: ActorImage;
  thumbnail?: ActorImage;
  hero?: ActorImage;
  avatar?: ActorImage;
};

async function loadActor(actorId: string): Promise<ActorImages> {
  const q = `
  query ($id: String!) {
    getActorById(id: $id) {
      _id
      thumbnail {
        _id
        name
        color
      }
      avatar {
        _id
        name
        color
      }
      altThumbnail {
        _id
        name
        color
      }
      hero {
        _id
        name
        color
      }
    }
  }
  `;

  const { getActorById } = await graphqlQuery<{
    getActorById: ActorImages;
  }>(q, {
    id: actorId,
  });

  return {
    thumbnail: getActorById.thumbnail,
    altThumbnail: getActorById.altThumbnail,
    hero: getActorById.hero,
    avatar: getActorById.avatar,
  };
}

type ImageEditorProps = {
  type: imageTypes;
  image?: ActorImage;
  uploading: boolean;
  onRemove: () => void;
  onImageUpload: (blob: Blob, type: string) => void;
};

function ImageEditControls({ type, image, onRemove, uploading, onImageUpload }: ImageEditorProps) {
  const t = useTranslations();
  const { blur: safeModeBlur } = useSafeMode();
  const { actorImageAspect, actorHeroAspect } = useSettings();

  const [fileToUpload, setFileToUpload] = useState<{
    buffer: string;
    type: imageTypes;
    name?: string;
  } | null>();

  const aspect = {
    hero: actorHeroAspect.numericValue,
    thumbnail: actorImageAspect.numericValue,
    altThumbnail: actorImageAspect.numericValue,
    avatar: 1,
  }[type];

  function changeImage(buffer: string, type: imageTypes, name?: string): void {
    setFileToUpload({ buffer, type, name });
  }

  function handleOnChange(event: ProgressEvent<FileReader>, filename?: string) {
    const fileReader = event.target as FileReader;

    const name = filename || image?.name;

    if (!fileReader.result) {
      return;
    }

    if (fileReader.result instanceof ArrayBuffer) {
      const base64 = btoa(String.fromCharCode(...new Uint8Array(fileReader.result)));
      changeImage(base64, type, name);
    } else {
      changeImage(fileReader.result, type, name);
    }
  }

  function doUpload(blob: Blob) {
    if (fileToUpload?.type) {
      onImageUpload(blob, fileToUpload.type);
    }
    setFileToUpload(null);
  }

  if (fileToUpload) {
    return (
      <Window onClose={() => setFileToUpload(null)} isOpen={true} title="Edit image">
        <div className="flex flex-col gap-2">
          <div className="text-gray-500 dark:text-gray-500">
            {t(`imageType.description.${type}`)}
          </div>
          <ImageCropper
            aspectRatio={aspect}
            src={fileToUpload.buffer}
            loading={uploading}
            onCancel={() => setFileToUpload(null)}
            onUpload={doUpload}
          />
        </div>
      </Window>
    );
  }

  return (
    <>
      {image?._id ? (
        <div>
          <div className="text-sm text-gray-600 dark:text-gray-500">{t(`imageType.${type}`)}</div>
          <div className="flex items-center justify-center gap-2">
            <img
              className="h-[120px] w-[120px]"
              src={imageUrl(image._id)}
              style={{ filter: safeModeBlur, objectFit: "contain" }}
            />
            <div className="flex flex-col gap-2">
              <FileInput
                onChange={(files) => {
                  if (files?.length) {
                    const fileReader = new FileReader();
                    fileReader.onload = handleOnChange;
                    fileReader.readAsDataURL(files[0]);
                  }
                }}
              >
                Choose
              </FileInput>
              <Button secondary onClick={onRemove}>
                Remove
              </Button>
            </div>
          </div>
        </div>
      ) : (
        <div>
          <div className="text-sm text-gray-600 dark:text-gray-500">{t(`imageType.${type}`)}</div>
          <div className="flex items-center justify-center gap-2">
            <div
              className="h-[120px] w-[120px]"
              style={{
                background: `repeating-linear-gradient(
                  45deg,
                  #a0a0a005,
                  #a0a0a005 10px,
                  #a0a0a010 10px,
                  #a0a0a010 20px
                )`,
                border: "2px solid #a0a0a020",
              }}
            />
            <FileInput
              onChange={(files) => {
                if (files?.length) {
                  const fileReader = new FileReader();
                  fileReader.onload = (e) => handleOnChange(e, files[0].name);
                  fileReader.readAsDataURL(files[0]);
                }
              }}
            >
              Choose
            </FileInput>
          </div>
        </div>
      )}
    </>
  );
}

type ActorImagesEditorProps = {
  actorId: string;
  onClose?: () => void;
};

export default function ActorImagesEditor({ actorId, onClose }: ActorImagesEditorProps) {
  const t = useTranslations();
  const { isOpen, close, open } = useWindow();
  const [_loading, setLoader] = useState(false);
  const [uploading, setUploading] = useState(false);

  const [avatar, setAvatar] = useState<ActorImage>();
  const [hero, setHero] = useState<ActorImage>();
  const [altThumbnail, setAltThumbnail] = useState<ActorImage>();
  const [thumbnail, setThumbnail] = useState<ActorImage>();

  function doClose() {
    close();
    onClose?.();
  }

  async function removeImage(type: string): Promise<void> {
    if (!window.confirm(`Are you sure to remove the ${type} image?`)) {
      return;
    }

    switch (type) {
      case "avatar":
        await setActorAvatar(actorId, null);
        setAvatar(undefined);
        break;
      case "thumbnail":
        await setActorThumbnail(actorId, null);
        setThumbnail(undefined);
        break;
      case "altThumbnail":
        await setActorAltThumbnail(actorId, null);
        setAltThumbnail(undefined);
        break;
      case "hero":
        await setActorHero(actorId, null);
        setHero(undefined);
        break;
    }
  }

  async function onImageUpload(blob: Blob, type: string) {
    setUploading(true);
    const uploadedImage = await uploadImage({ blob, name: `uploaded (${type})` }, [actorId]);
    setUploading(false);

    switch (type) {
      case "avatar": {
        const newImage = await setActorAvatar(actorId, uploadedImage._id);
        setAvatar(newImage);
        break;
      }
      case "thumbnail": {
        const newImage = await setActorThumbnail(actorId, uploadedImage._id);
        setThumbnail(newImage);
        break;
      }
      case "altThumbnail": {
        const newImage = await setActorAltThumbnail(actorId, uploadedImage._id);
        setAltThumbnail(newImage);
        break;
      }
      case "hero": {
        const newImage = await setActorHero(actorId, uploadedImage._id);
        setHero(newImage);
        break;
      }
    }
  }

  useMount(() => {
    setLoader(true);
    loadActor(actorId)
      .then((result) => {
        setAvatar(result.avatar);
        setHero(result.hero);
        setThumbnail(result.thumbnail);
        setAltThumbnail(result.altThumbnail);
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => setLoader(false));
  });

  return (
    <>
      <Button onClick={open}>{t("action.editImages")}</Button>
      <Window
        onClose={doClose}
        isOpen={isOpen}
        title={t("action.editImages")}
        actions={
          <>
            <Button secondary onClick={doClose}>
              Close
            </Button>
          </>
        }
      >
        <Flex className="grid grid-cols-1 gap-5 md:grid-cols-2">
          <ImageEditControls
            uploading={uploading}
            onImageUpload={onImageUpload}
            type="thumbnail"
            image={thumbnail}
            onRemove={async () => removeImage("thumbnail")}
          />
          <ImageEditControls
            uploading={uploading}
            onImageUpload={onImageUpload}
            type="altThumbnail"
            image={altThumbnail}
            onRemove={async () => removeImage("altThumbnail")}
          />
          <ImageEditControls
            uploading={uploading}
            onImageUpload={onImageUpload}
            type="avatar"
            image={avatar}
            onRemove={async () => removeImage("avatar")}
          />
          <ImageEditControls
            uploading={uploading}
            onImageUpload={onImageUpload}
            type="hero"
            image={hero}
            onRemove={async () => removeImage("hero")}
          />
        </Flex>
      </Window>
    </>
  );
}
