export interface IStudio {
  _id: string;
  addedOn: number;
  name: string;
  aliases?: string[];
  description?: string;
  favorite: boolean;
  bookmark: boolean;
  averageRating: number;
  thumbnail?: {
    _id: string;
  };
  parent?: {
    _id: string;
    name: string;
    thumbnail?: {
      _id: string;
    };
  };
  substudios: IStudio[];
  labels: {
    _id: string;
    name: string;
    color?: string;
  }[];
  numScenes: number;
}
