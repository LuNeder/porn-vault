import { execaSync } from "execa";
import { type Application } from "express";

import args from "./args";
import { imagemagickHelp } from "./binaries/imagemagick";
import {
  deleteIzzy,
  ensureIzzyExists,
  exitIzzy,
  izzyFilename,
  izzyHasRequiredVersion,
  izzyPath,
  izzyVersion,
  requiredIzzyVersion,
  spawnIzzy,
} from "./binaries/izzy";
import { generateMissingMarkerThumbnails, generateMissingScenePreviewGrids, generateMissingScenePreviewStrips } from "./commands";
import { checkConfig, findAndLoadConfig, getConfig } from "./config";
import { IConfig } from "./config/schema";
import { loadStores } from "./database";
import { loadEnv } from "./env";
import { repairIntegrity } from "./integrity";
import { runMigrations } from "./migrations";
import { initializePlugins } from "./plugins/register";
import { tryStartProcessing } from "./queue/processing";
import { ensureIndices, } from "./search";
import { createVaultLogger, formatMessage, handleError, logger, setLogger } from "./utils/logger";
import { version } from "./version";

async function checkIzzyVersion(port: number) {
  if (!(await izzyHasRequiredVersion(port))) {
    logger.error(
      `Izzy does not satisfy version: ${requiredIzzyVersion} (is at version ${await izzyVersion(port)})`
    );
    logger.info(
      `Use --update-izzy, delete ${izzyFilename} and restart or download manually from https://gitlab.com/porn-vault/izzy/-/releases and save at ${izzyPath}`
    );
    logger.debug("Killing izzy...");
    await exitIzzy(port);
    process.exit(1);
  }
}

async function startDatabase(config: IConfig) {
  try {
    logger.verbose("Making sure database executable exists");
    await ensureIzzyExists();

    logger.info("Loading database");
    const iVersion = await izzyVersion(config.binaries.izzyPort).catch(() => false);

    if (iVersion) {
      // Izzy is running

      await checkIzzyVersion(config.binaries.izzyPort);
      logger.info(`Izzy ${iVersion} already running (on port ${config.binaries.izzyPort})...`);

      if (args["reload-izzy"]) {
        logger.warn("Reloading izzy");
        await exitIzzy(config.binaries.izzyPort);
        await spawnIzzy(config.binaries.izzyPort);
      } else {
        logger.info("Using existing Izzy process, will not be able to detect a crash");
      }
    } else {
      await spawnIzzy(config.binaries.izzyPort);
    }

    await checkIzzyVersion(config.binaries.izzyPort);
  } catch (err) {
    handleError("Error setting up Izzy", err, true);
  }
}

export async function startupVault(app: Application): Promise<void> {
  console.error(
    `${"=".repeat(40)}
Porn-Vault ${version} Copyright (C) ${new Date().getFullYear()}
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it
under certain conditions, see: https://gitlab.com/porn-vault/porn-vault/-/blob/dev/LICENSE
${"=".repeat(40)}`
  );

  logger.debug("Startup vault");
  logger.info("Check https://gitlab.com/porn-vault/porn-vault for discussion & updates");

  loadEnv();

  let config: IConfig;

  try {
    const shouldRestart = await findAndLoadConfig();
    if (shouldRestart) {
      process.exit(0);
    }

    config = getConfig();
    checkConfig(config);
  } catch (err) {
    return handleError(`Error during startup`, err, true);
  }

  logger.debug("Refreshing logger");
  setLogger(createVaultLogger(config.log.level, config.log.writeFile));

  logger.debug(formatMessage(args));

  const magickBins = ["convert", "montage", "identify"] as const;

  for (const bin of magickBins) {
    const path = config.binaries.imagemagick[`${bin}Path`];
    logger.verbose(`Checking imagemagick (${bin})...`);

    try {
      execaSync(path, ["--version"]);
    } catch (err) {
      imagemagickHelp(bin, path);
      return handleError(`Failed to run imagemagick (${bin}) at ${path}`, err, true);
    }
  }

  if (args.migrate) {
    logger.verbose("Starting migrations because --migrations = true");

    await startDatabase(config);
    await loadStores().catch((error) => {
      handleError(
        `Error while loading database, try restarting; if the error persists, your database may be corrupted`,
        error,
        true
      );
    });

    await runMigrations();

    logger.info("Done, please restart");
    process.exit(0);
  }

  /* TODO: RESTORE backup */

  if (args.updateIzzy) {
    await deleteIzzy();
    await ensureIzzyExists();
  }

  await startDatabase(config);

  await loadStores().catch((error) => {
    handleError(
      `Error while loading database, try restarting; if the error persists, your database may be corrupted`,
      error,
      true
    );
  });

  if (args.repairIntegrity) {
    await repairIntegrity();
    logger.info("Integrity repair done, please restart");
    process.exit(0);
  }

  if (args.generateMissingScenePreviewStrips) {
    await generateMissingScenePreviewStrips();
    logger.info(`generateMissingScenePreviewStrips done`);
  }
  if (args.generateMissingScenePreviewGrids) {
    await generateMissingScenePreviewGrids();
    logger.info(`generateMissingScenePreviewGrids done`);
  }
  if (args.generateMissingMarkerThumbnails) {
    await generateMissingMarkerThumbnails();
    logger.info(`generateMissingMarkerThumbnails done`);
  }

  try {
    logger.info("Loading search engine");
    await ensureIndices(config, args.reindex);
  } catch (error) {
    handleError("Error while loading search engine", error, true);
  }

  await initializePlugins(config);

  tryStartProcessing().catch((err: Error) => {
    handleError("Couldn't start processing", err);
  });
}
