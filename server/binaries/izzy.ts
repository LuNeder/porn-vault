import { ChildProcess, spawn } from "node:child_process";
import { chmodSync, existsSync } from "node:fs";
import { arch, type } from "node:os";
import { basename } from "node:path";

import Axios from "axios";
import semver from "semver";

import { downloadFile } from "../utils/download";
import { unlinkAsync } from "../utils/fs/async";
import { formatMessage, handleError, logger } from "../utils/logger";
import { configPath } from "../utils/path";

export let izzyProcess!: ChildProcess;

export const izzyFilename = process.env.IZZY_PATH
  ? basename(process.env.IZZY_PATH)
  : configPath(type() === "Windows_NT" ? "izzy.exe" : "izzy");

export const izzyPath =
  process.env.IZZY_PATH || configPath(type() === "Windows_NT" ? "izzy.exe" : "izzy");

export const izzyHost: string = process.env.IZZY_HOST || "localhost";

export const requiredIzzyVersion = "^2.0.1";

export async function deleteIzzy(): Promise<void> {
  logger.verbose("Deleting izzy");
  await unlinkAsync(izzyPath);
}

export async function resetIzzy(port: number): Promise<void> {
  try {
    logger.verbose("Resetting izzy");
    await Axios.delete(`http://${izzyHost}:${port}/collection`);
  } catch (error) {
    handleError(`Error while resetting izzy`, error);
    throw error;
  }
}

export async function exitIzzy(port: number): Promise<void> {
  logger.verbose("Closing izzy");
  await Axios.post(`http://${izzyHost}:${port}/exit`);
}

export async function izzyHasRequiredVersion(port: number): Promise<boolean> {
  const version = await izzyVersion(port).catch(() => null);
  if (!version) {
    logger.error("Could not ping izzy for version");
    return false;
  }
  return semver.satisfies(version, requiredIzzyVersion);
}

export async function izzyVersion(port: number): Promise<string> {
  logger.debug("Getting izzy version");
  const res = await Axios.get<{ version: string }>(
    `http://${izzyHost}:${port}/`
  );
  return res.data.version;
}

const URL = process.env.IZZY_URL || "https://gitlab.com/api/v4/projects/31639446/releases";

async function downloadIzzy() {
  const _type = type();
  const _arch = arch();

  const downloadName = {
    Windows_NT: "izzy_win",
    Linux: "izzy_linux",
    Darwin: "izzy_mac",
  }[_type] as string;

  if (_arch !== "x64") {
    throw new Error(`Unsupported architecture ${_arch}`);
  }

  logger.verbose("Fetching Izzy releases");
  const { data: releases } = await Axios.get<
    {
      tag_name: string;
      assets: {
        links: { name: string; url: string }[];
      };
    }[]
  >(URL);

  const latestMatchingReleases = releases.filter(({ tag_name }) =>
    semver.satisfies(tag_name, requiredIzzyVersion)
  );

  logger.silly(`Matching releases: ${formatMessage(latestMatchingReleases)}`);

  const [latestMatchingRelease] = latestMatchingReleases;

  if (!latestMatchingRelease) {
    throw new Error(`No matching release found for ${requiredIzzyVersion}`);
  }

  logger.silly(latestMatchingRelease);
  logger.info(`Fetching release ${latestMatchingRelease.tag_name}`);

  const asset = latestMatchingRelease.assets.links.find(
    (as) => as.name.includes(downloadName) && as.name.includes(_arch)
  );

  if (!asset) {
    logger.silly(latestMatchingRelease.assets.links);
    throw new Error(
      `Izzy release does not support your machine: ${downloadName} for ${_type} ${_arch}`
    );
  }

  await downloadFile(asset.url, izzyPath);
}

export async function ensureIzzyExists(): Promise<0 | 1> {
  if (existsSync(izzyPath)) {
    logger.debug("Izzy binary found");
    return 0;
  } else {
    logger.info("Downloading latest Izzy (database) binary");
    await downloadIzzy();
    return 1;
  }
}

export function spawnIzzy(port: number): Promise<void> {
  return new Promise((resolve, reject) => {
    logger.debug("CHMOD Izzy");
    chmodSync(izzyPath, "111");

    logger.debug(`Spawning Izzy (${izzyPath}) on port ${port}`);

    izzyProcess = spawn(izzyPath, ["--port", port.toString()], {
      detached: true,
      stdio: "ignore",
    });

    izzyProcess
      .on("error", (err: Error) => {
        reject(err);
      })
      .on("exit", (code: number) => {
        logger.error(`Izzy exited with code ${code}, will exit`);
        process.exit(1);
      });

    izzyProcess.unref();
    setTimeout(resolve, 2500);
  });
}
