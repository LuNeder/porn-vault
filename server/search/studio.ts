import { indexes, StudioSearchDoc } from "../search_new";
import Studio from "../types/studio";
import { mapAsync } from "../utils/async";
import { buildIndex, indexItems, ProgressCallback } from "./internal/buildIndex";

export async function createStudioSearchDoc(studio: Studio): Promise<StudioSearchDoc> {
  const labels = await Studio.getLabels(studio);
  const parentStudios = await Studio.getParents(studio);

  return {
    _id: studio._id,
    addedOn: studio.addedOn,
    name: studio.name,
    aliases: studio.aliases ?? [],
    parents: parentStudios.map((x) => x._id),
    parentNames: parentStudios.map((x) => x.name),
    rawName: studio.name,
    labels: labels.map((l) => l._id),
    labelNames: labels.map((l) => l.name),
    numLabels: labels.length,
    rating: 0,
    averageRating: Math.floor(await Studio.getAverageRating(studio)),
    bookmark: studio.bookmark ?? 0,
    favorite: studio.favorite,
    numScenes: (await Studio.getScenes(studio)).length,
    custom: studio.customFields,
  };
}

async function addStudioSearchDocs(docs: StudioSearchDoc[]) {
  await indexes.studios.indexItems(
    docs
  );
}

export async function removeStudio(studioId: string): Promise<void> {
  await indexes.studios.removeItem(studioId);
}

export async function removeStudios(studioIds: string[]): Promise<void> {
  // TODO: bulk delete
  await mapAsync(studioIds, removeStudio);
}

export async function indexStudios(
  studios: Studio[],
  progressCb?: ProgressCallback
): Promise<number> {
  return indexItems(studios, createStudioSearchDoc, addStudioSearchDocs, progressCb);
}

export async function buildStudioIndex(): Promise<void> {
  await buildIndex(indexes.studios.name, Studio.getAll, indexStudios);
}

export interface IStudioSearchQuery {
  query: string;
  favorite?: boolean;
  bookmark?: boolean;
  // rating: number;
  include?: string[];
  exclude?: string[];
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;

  rawQuery?: unknown; // TODO:
}
