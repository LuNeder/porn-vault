import { IConfig } from "../config/schema";
import { ensureSearchIndexesExist, indexes } from "../search_new";
import { buildActorIndex } from "./actor";
import { buildImageIndex } from "./image";
import { buildMarkerIndex } from "./marker";
import { buildMovieIndex } from "./movie";
import { buildSceneIndex } from "./scene";
import { buildStudioIndex } from "./studio";

export async function clearIndices() {
  for (const indexName in indexes) {
    const index = indexes[indexName as keyof typeof indexes];
    await index.deleteIndex();
  }
}

export async function ensureIndices(config: IConfig, wipeData: boolean) {
  if (wipeData) {
    await clearIndices();
  }

  await ensureSearchIndexesExist(config);

  if (wipeData) {
    await buildActorIndex();
    await buildSceneIndex();
    await buildStudioIndex();
    await buildMovieIndex();
    await buildImageIndex();
    await buildMarkerIndex();
  }
}
