import { indexes, SceneSearchDoc } from "../search_new";
import Movie from "../types/movie";
import Scene from "../types/scene";
import Studio from "../types/studio";
import SceneView from "../types/watch";
import { mapAsync } from "../utils/async";
import { logger } from "../utils/logger";
import {
  getActorNames,
} from "./common";
import { buildIndex, indexItems, ProgressCallback } from "./internal/buildIndex";

async function createSceneSearchDoc(scene: Scene): Promise<SceneSearchDoc> {
  const labels = await Scene.getLabels(scene);
  const actors = await Scene.getActors(scene);
  const movies = await Movie.getByScene(scene._id);

  const watches = await SceneView.getByScene(scene._id);

  const studio = scene.studio ? await Studio.getById(scene.studio) : null;
  const parentStudios = studio ? await Studio.getParents(studio) : [];

  return {
    _id: scene._id,
    addedOn: scene.addedOn,
    name: scene.name,
    rawName: scene.name,
    path: scene.path ?? "",
    rawPath: scene.path ?? "",
    labels: labels.map((l) => l._id),
    numLabels: labels.length,
    actors: actors.map((a) => a._id),
    numActors: actors.length,
    actorNames: [...new Set(actors.map(getActorNames).flat())],
    labelNames: labels.map((l) => l.name),
    rating: scene.rating,
    bookmark: scene.bookmark ?? 0,
    favorite: scene.favorite,
    numViews: watches.length,
    lastViewedOn: watches.sort((a, b) => b.date - a.date)[0]?.date || 0,
    duration: Math.floor(scene.meta.duration ?? 0),
    releaseDate: scene.releaseDate ?? 0,
    releaseYear: scene.releaseDate ? new Date(scene.releaseDate).getFullYear() : 0,
    studios: studio ? [studio, ...parentStudios].map((s) => s._id) : [],
    resolution: scene.meta.dimensions?.height ?? 0,
    size: scene.meta.size ?? 0,
    studioNames: studio
      ? [...new Set([studio, ...parentStudios].flatMap((s) => [s.name, ...(s.aliases ?? [])]))]
      : [],
    score: Math.floor(Scene.calculateScore(scene, watches.length)),
    movies: movies.map(x => x._id),
    movieNames: movies.map((m) => m.name),
    numMovies: movies.length,
    custom: scene.customFields,
    bitrate: Math.floor(scene.meta.bitrate ?? 0),
  };
}

export async function buildSceneIndex(): Promise<void> {
  await buildIndex(indexes.scenes.name, Scene.getAll, indexScenes);
}

async function addSceneSearchDocs(docs: SceneSearchDoc[]) {
  await indexes.scenes.indexItems(
    docs
  );
}

export async function indexScenes(scenes: Scene[], progressCb?: ProgressCallback): Promise<number> {
  logger.verbose(`Indexing ${scenes.length} scenes`);
  return indexItems(scenes, createSceneSearchDoc, addSceneSearchDocs, progressCb);
}

export async function removeScene(sceneId: string): Promise<void> {
  await indexes.scenes.removeItem(sceneId);
}

export async function removeScenes(sceneIds: string[]): Promise<void> {
  // TODO: bulk delete
  await mapAsync(sceneIds, removeScene);
}

export interface ISceneSearchQuery {
  id: string;
  query: string;
  favorite?: boolean;
  bookmark?: boolean;
  unwatchedOnly: boolean;
  rating: number;
  include?: string[];
  exclude?: string[];
  studios?: string[];
  actors?: string[];
  sortBy?: string;
  sortDir?: string;
  skip?: number;
  take?: number;
  page?: number;
  durationMin?: number;
  durationMax?: number;

  rawQuery?: unknown; // TODO:
}
