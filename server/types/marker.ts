import * as path from "node:path";

import { takeScreenshot } from "../binaries/ffmpeg";
import { collections } from "../database";
import { generateHash } from "../utils/hash";
import { logger } from "../utils/logger";
import { getFolderPartition, libraryPath } from "../utils/path";
import { normalizeName } from "../utils/string";
import Actor from "./actor";
import ActorReference from "./actor_reference";
import { iterate } from "./common";
import Image from "./image";
import Label from "./label";
import Scene from "./scene";

const MARKER_LOOKAHEAD_SECS = 15;

export default class Marker {
  _id: string;
  name: string;
  addedOn = +new Date();
  favorite = false;
  bookmark: number | null = null;
  rating = 0;
  customFields: Record<string, boolean | string | number | string[] | null> = {};
  scene: string;
  time: number; // Time in scene in seconds
  thumbnail?: string | null = null;

  static async iterate(
    func: (scene: Marker) => void | unknown | Promise<void | unknown>,
    extraFilter: unknown[] = []
  ) {
    return iterate(async () => ({ items: [] }), Marker.getBulk, func, "marker", extraFilter);
  }

  static async getAll(): Promise<Marker[]> {
    return collections.markers.getAll();
  }

  static async createMarkerThumbnail(marker: Marker): Promise<Partial<Marker>> {
    if (marker.thumbnail) {
      logger.warn("Marker already has thumbnail");
      return {};
    }

    const scene = await Scene.getById(marker.scene);
    if (!scene || !scene.path) {
      logger.warn(`Marker has no scene (path) ${marker.scene}`);
      return {};
    }

    logger.verbose(`Creating thumbnail for marker ${marker._id}`);
    const image = new Image(`${marker.name} (thumbnail)`);
    const imagePath = path.resolve(
      getFolderPartition(libraryPath("thumbnails/markers")),
      `${image._id}.jpg`
    );
    image.path = imagePath;
    image.scene = marker.scene;

    await takeScreenshot(scene.path, (marker.time + MARKER_LOOKAHEAD_SECS) * 1000, imagePath, 480);
    await collections.images.upsert(image._id, image);

    return {
      thumbnail: image._id
    }
  }

  static async getActors(marker: Marker): Promise<Actor[]> {
    const references = await ActorReference.getByItem(marker._id);
    return (await collections.actors.getBulk(references.map((r) => r.actor))).sort((a, b) =>
      a.name.localeCompare(b.name)
    );
  }

  static async setActors(marker: Marker, actorIds: string[]): Promise<void> {
    return Actor.setForItem(marker._id, actorIds, "marker");
  }

  static async addActors(marker: Marker, actorIds: string[]): Promise<void> {
    return Actor.addForItem(marker._id, actorIds, "marker");
  }

  static async addLabels(marker: Marker, labelIds: string[]): Promise<void> {
    return Label.addForItem(marker._id, labelIds, "marker");
  }

  static async setLabels(marker: Marker, labelIds: string[]): Promise<void> {
    return Label.setForItem(marker._id, labelIds, "marker");
  }

  static async getLabels(marker: Marker): Promise<Label[]> {
    return Label.getForItem(marker._id);
  }

  constructor(name: string, scene: string, time: number) {
    this._id = `mk_${generateHash()}`;
    this.name = normalizeName(name);
    this.scene = scene;
    this.time = Math.round(time);
  }

  static async getAtTime(sceneId: string, time: number, threshold: number) {
    const markers = await Marker.getByScene(sceneId);
    return markers.find((m) => Math.abs(m.time - time) < threshold);
  }

  static async getByScene(sceneId: string): Promise<Marker[]> {
    return collections.markers.query("scene-index", sceneId);
  }

  static async getById(_id: string): Promise<Marker | null> {
    return collections.markers.get(_id);
  }

  static getBulk(_ids: string[]): Promise<Marker[]> {
    return collections.markers.getBulk(_ids);
  }

  static async remove(_id: string): Promise<void> {
    await collections.markers.remove(_id);
  }

  static async removeByScene(sceneId: string): Promise<void> {
    await collections.markers.deleteByQuery("scene-index", sceneId);
  }
}
