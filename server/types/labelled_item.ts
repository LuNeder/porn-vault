import { collections } from "../database/index";
import { BatchLabelLoader } from "../graphql/datasources/loaders";
import { generateHash } from "../utils/hash";

export default class LabelledItem {
  _id: string;
  label: string;
  item: string;
  type: "scene" | "actor" | "marker" | "image" | "studio";

  constructor(
    item: string,
    label: string,
    type: "scene" | "actor" | "marker" | "image" | "studio"
  ) {
    this._id = `li_${generateHash()}`;
    this.item = item;
    this.label = label;
    this.type = type;
  }

  static async getAll(): Promise<LabelledItem[]> {
    return collections.labelledItems.getAll();
  }

  static async getByLabel(label: string): Promise<LabelledItem[]> {
    return collections.labelledItems.query("label-index", label);
  }

  static async getByItem(item: string): Promise<LabelledItem[]> {
    return collections.labelledItems.query("item-index", item);
  }

  static async getByItemBulk(item: readonly string[]): Promise<Record<string, LabelledItem[]>> {
    return collections.labelledItems.queryBulk("item-index", item);
  }

  static async get(from: string, to: string): Promise<LabelledItem | undefined> {
    const fromReferences = await collections.labelledItems.query("item-index", from);
    return fromReferences.find((r) => r.label === to);
  }

  static async removeByLabel(labelId: string): Promise<void> {
    await collections.labelledItems.deleteByQuery("label-index", labelId);
    BatchLabelLoader.clearAll();
  }

  static async removeByItem(itemId: string): Promise<void> {
    await collections.labelledItems.deleteByQuery("item-index", itemId);
    BatchLabelLoader.clear(itemId);
  }

  static async remove(itemId: string, labelId: string): Promise<void> {
    const ref = await LabelledItem.get(itemId, labelId);
    if (ref) {
      await LabelledItem.removeById(ref._id);
      BatchLabelLoader.clear(itemId);
    }
  }

  static async removeById(id: string): Promise<void> {
    await collections.labelledItems.remove(id);
  }
}
