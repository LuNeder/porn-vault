import { existsSync } from "node:fs";
import { uptime } from "node:os";

import { Router } from "express";

import { exitIzzy, izzyVersion } from "../binaries/izzy";
import { getConfig } from "../config";
import {
  collectionBuildInfoMap,
  CollectionBuildStatus,
  collectionDefinitions,
  collections,
  formatCollectionName,
} from "../database";
import { clearCaches } from "../graphql/datasources";
import { ensureIndices } from "../search";
import { statAsync } from "../utils/fs/async";
import { handleError, logger } from "../utils/logger";
import { libraryPath } from "../utils/path";

enum ServiceStatus {
  Unknown = "unknown",
  Disconnected = "disconnected",
  Stopped = "stopped",
  Connected = "connected",
}

const router = Router();

/* router.get("/status/simple", (req, res) => {
  res.json({
    serverReady: vault?.serverReady || false,
    setupMessage: vault?.setupMessage || "",
  });
}); */

router.get("/status/full", async (req, res) => {
  let izzyStatus = ServiceStatus.Unknown;
  let iVersion = "unknown";

  const izzyPromise = new Promise<void>((resolve) =>
    (async () => {
      try {
        iVersion = await izzyVersion(getConfig().binaries.izzyPort);
        izzyStatus = ServiceStatus.Connected;
      } catch (err) {
        handleError("Error getting Izzy info", err);
        /* if (!axios.isAxiosError(err)) {
          izzyStatus = ServiceStatus.Unknown;
        } else if (err.response) {
          izzyStatus = ServiceStatus.Disconnected;
        } else {
          izzyStatus = ServiceStatus.Stopped;
        } */
      }
      resolve();
    })()
  );

  const izzyCollections: { name: string; count: number; size: number; path: string }[] = [];
  const izzyCollectionsPromise = new Promise<void>((resolve) =>
    (async () => {
      try {
        const collectionInfoPromises = Object.values(collectionDefinitions).map(
          async (collectionDef): Promise<void> => {
            const name = formatCollectionName(collectionDef.name);
            if (collectionBuildInfoMap[collectionDef.key].status === CollectionBuildStatus.None) {
              izzyCollections.push({
                name,
                count: 0,
                size: 0,
                path: "",
              });
              return;
            }

            const count = await collections[collectionDef.key].count();

            const path = libraryPath(collectionDef.path);

            if (!existsSync(path)) {
              izzyCollections.push({
                name,
                count: 0,
                size: 0,
                path,
              });
            } else {
              const { size } = await statAsync(path);
              izzyCollections.push({
                name,
                count,
                size,
                path,
              });
            }
          }
        );
        await Promise.all(collectionInfoPromises);
        izzyCollections.sort((a, b) => a.name.localeCompare(b.name));
      } catch (err) {
        handleError("Error getting Izzy collections info", err);
      }
      resolve();
    })()
  );

  await Promise.all([izzyPromise, izzyCollectionsPromise]);

  const serverUptime = process.uptime();
  const osUptime = uptime();

  res.json({
    izzy: {
      status: izzyStatus,
      version: iVersion,
      collections: izzyCollections,
      collectionBuildInfoMap, // load progress
      allCollectionsBuilt: Object.keys(collections).every(
        (collectionKey) =>
          collectionBuildInfoMap[collectionKey].status === CollectionBuildStatus.Ready
      ),
    },

    // Other
    serverUptime,
    osUptime,
    /* serverReady: vault?.serverReady || false,
    setupMessage: vault?.setupMessage || "", */
  });
});

router.post("/exit", async (req, res) => {
  res.status(200).send("Exiting...");

  const stopIzzy = req.body as { stopIzzy?: boolean };

  logger.info(`Exiting porn-vault. Will ${stopIzzy ? "exit Izzy" : "keep Izzy running"}.`);
  /* setServerStatus(false, "Exiting..."); */

  if (stopIzzy) {
    await exitIzzy(getConfig().binaries.izzyPort).catch((err) => {
      handleError("Error exiting izzy", err);
    });
  }

  process.exit(0);
});

router.post("/reindex", async (_, res) => {
  res.status(200).send("Reindexing...");
  try {
    logger.info("Reindexing...");
    /* setServerStatus(false, "Loading search engine..."); */

    clearCaches();
    await ensureIndices(getConfig(), true);

    /* setServerStatus(true, "Ready"); */
  } catch (err) {
    handleError("Error reindexing from user request", err);
  }
});

router.delete("/cache", (_, res) => {
  clearCaches();
  res.json({});
});

export default router;
