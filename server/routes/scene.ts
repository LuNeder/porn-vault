import { randomUUID } from "node:crypto";
import { existsSync } from "node:fs";
import path from "node:path";

import { execa } from "execa";
import { Request, Response, Router } from "express";

import { multiScreenshot } from "../binaries/ffmpeg";
import { getConfig } from "../config";
import { collections } from "../database";
/* import { CopyMP4Transcoder } from "../transcode/copyMp4";
import { MP4Transcoder } from "../transcode/mp4";
import { SceneStreamTypes, TranscodeOptions } from "../transcode/transcoder";
import { WebmTranscoder } from "../transcode/webm"; */
import Scene from "../types/scene";
import { mkdirpAsync, readFileAsync, rimrafAsync } from "../utils/fs/async";
import { handleError, logger } from "../utils/logger";
import { generateTimestampsAtIntervals } from "../utils/misc";
import { getFolderPartition, libraryPath, tempPath } from "../utils/path";
import { IMAGE_CACHE_CONTROL } from "./media";

/* function streamTranscode(
  scene: Scene & { path: string },
  req: Request,
  res: Response,
  options: TranscodeOptions
): void {
  res.writeHead(200, {
    "Accept-Ranges": "bytes",
    Connection: "keep-alive",
    "Transfer-Encoding": "chunked",
    "Content-Disposition": "inline",
    "Content-Transfer-Enconding": "binary",
    "Content-Type": options.mimeType,
  });

  const startQuery = (req.query as { start?: string }).start || "0";
  const startSeconds = Number.parseFloat(startQuery);
  if (Number.isNaN(startSeconds)) {
    res
      .status(400)
      .send(`Could not parse start query as number: ${startQuery}`);
    return;
  }

  options.inputOptions.unshift(`-ss ${startSeconds}`);

  // Time out the request after 2mn to prevent accumulating
  // too many ffmpeg processes. After that, the user should reload the page
  req.setTimeout(2 * 60 * 1000);

  let command: ffmpeg.FfmpegCommand | null = null;

  command = ffmpeg(scene.path)
    .inputOptions(options.inputOptions)
    .outputOptions(options.outputOptions)
    .on("start", (commandLine: string) => {
      logger.verbose(`Spawned Ffmpeg with command: ${commandLine}`);
    })
    .on("end", () => {
      logger.verbose(`Scene "${scene.path}" has been converted successfully`);
    })
    .on("error", (err) => {
      handleError(
        `Request finished or an error happened while transcoding scene "${scene.path}"`,
        err
      );
    });

  res.on("close", () => {
    logger.verbose("Stream request closed, killing transcode");
    command?.kill("SIGKILL");
  });

  command.pipe(res, { end: true });
} */

function streamDirect(scene: Scene & { path: string }, _: Request, res: Response): Response | void {
  const resolved = path.resolve(scene.path);
  return res.sendFile(resolved);
}

const router = Router();

const rows = 6;
const cols = 4;
const size = 200;

export async function attachScenePreviewGrid(scene: Scene): Promise<string | null> {
  if (!scene.path) {
    return null;
  }
  if (!scene.meta.duration) {
    return null;
  }

  const gridFolder = path.resolve(tempPath, "grid");
  const tmpFolder = path.resolve(gridFolder, "thumbs", randomUUID());
  await mkdirpAsync(tmpFolder);

  const timestamps = generateTimestampsAtIntervals(rows * cols, scene.meta.duration * 1000, {
    startPercentage: 2,
    endPercentage: 100,
  });

  logger.debug(`Creating grid screenshots for ${scene._id}`);
  const files = await multiScreenshot({
    videoFile: scene.path,
    outputFolder: tmpFolder,
    timestamps,
    width: size,
  });

  const file = path.resolve(
    getFolderPartition(libraryPath("preview_grids")),
    `${scene._id}_${rows}_${cols}_${size}.jpg`
  );

  logger.debug(`Stitching preview grid ${file}`);
  await execa(getConfig().binaries.imagemagick.montagePath, [
    ...files.map(({ path }) => path),
    /* "-title",
        sc.name, */
    "-tile",
    `${cols}x${rows}`,
    "-geometry",
    "+0+0",
    file,
  ]);

  logger.silly(`Storing preview grid path in scene`);
  await collections.scenes.partialUpdate(scene._id, {
    previewGrid: file,
  });

  try {
    logger.silly("Running grid generation cleanup");
    await rimrafAsync(tmpFolder);
  } catch (error) {
    handleError("Failed to cleanup grid generation folder", error);
  }

  return file;
}

router.get("/:scene/grid", async (req, res, next) => {
  try {
    const sc = await Scene.getById(req.params.scene);
    if (!sc) {
      return next(404);
    }
    if (sc.previewGrid) {
      logger.silly(`Reading existing grid file at ${sc.previewGrid}`);
      const buffer = await readFileAsync(sc.previewGrid);
      res.set("Cache-control", IMAGE_CACHE_CONTROL);
      return res.status(200).setHeader("content-type", "image/jpg").send(buffer);
    }
    else {
      logger.debug(`Scene "${sc._id}" has no preview grid (yet)`);
    }

    const file = await attachScenePreviewGrid(sc);
    if (!file) {
      return next(404);
    }

    logger.silly(`Reading generated grid file at ${file}`);
    const buffer = await readFileAsync(file);
    res.set("Cache-control", IMAGE_CACHE_CONTROL);
    res.status(200).setHeader("content-type", "image/jpg").send(buffer);
  } catch (error) {
    handleError("Failed to create scene preview grid", error);
    res.sendStatus(500);
  }
});

router.get("/:scene", async (req, res, next) => {
  const sc = await Scene.getById(req.params.scene);
  if (!sc || !sc.path) {
    return next(404);
  }

  const scene = sc as Scene & { path: string };
  return streamDirect(scene, req, res);

  /* const streamType = (req.query as { type?: SceneStreamTypes }).type;

  if (!streamType || streamType === SceneStreamTypes.DIRECT) {
    return streamDirect(scene, req, res);
  }

  try {
    if (
      !scene.meta.container ||
      !scene.meta.videoCodec ||
      !scene.meta.bitrate
    ) {
      logger.verbose(
        `Scene ${scene._id} doesn't have codec information to determine supported transcodes, running ffprobe`
      );
      await Scene.applyVideoFileMetadata(scene);

      // Doesn't matter if this fails
      await collections.scenes.upsert(scene._id, scene).catch((err) => {
        handleError(
          "Failed to update scene after updating codec information",
          err
        );
      });
    }
  } catch (err) {
    handleError("Error getting video codecs for transcode", err);
    return res
      .status(500)
      .send("Could not determine video codecs for transcoding");
  }

  const TranscoderClass = {
    [SceneStreamTypes.MP4_DIRECT]: CopyMP4Transcoder,
    [SceneStreamTypes.MP4_TRANSCODE]: MP4Transcoder,
    [SceneStreamTypes.WEBM_TRANSCODE]: WebmTranscoder,
  }[streamType];
  if (!TranscoderClass) {
    return res.sendStatus(400);
  }
  const transcoder = new TranscoderClass(scene);

  const validateRes = transcoder.validateRequirements();
  if (validateRes !== true) {
    return res.status(400).send(validateRes.message);
  }

  const transcodeOpts = transcoder.getTranscodeOptions();
  return streamTranscode(scene, req, res, transcodeOpts); */
});

export default router;
