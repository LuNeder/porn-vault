import { toDays, toMonths, toSeconds, toYears } from "duration-fns";
import { Router } from "express";

import { collections } from "../database";
import SceneView from "../types/watch";
import { version } from "../version";
import configRouter from "./config";
import mediaRouter from "./media";
import scanRouter from "./scan";
import systemRouter from "./system";

const router = Router();

router.get("/version", (_, res) => res.send(version));
router.use("/scan", scanRouter);
router.use("/system", systemRouter);
router.use("/config", configRouter);
router.use("/media", mediaRouter);

router.get("/remaining-time", async (_req, res) => {
  const views = await SceneView.getAll();
  if (!views.length) {
    return res.json(null);
  }

  const now = Date.now();
  const numScenes = await collections.scenes.count();
  const viewedPercent = views.length / numScenes;
  const currentInterval = Math.floor(now - views.at(-1)!.date);
  const fullTime = currentInterval / viewedPercent;
  const remaining = Math.floor(fullTime - currentInterval);
  const remainingTimestamp = now + remaining;

  // TODO: server side cache result
  // clear cache when some scene viewed

  res.json({
    numViews: views.length,
    numScenes,
    viewedPercent,
    remaining,
    remainingSeconds: toSeconds({ milliseconds: remaining }),
    remainingDays: toDays({ milliseconds: remaining }),
    remainingMonths: toMonths({ milliseconds: remaining }),
    remainingYears: toYears({ milliseconds: remaining }),
    remainingTimestamp,
    currentInterval,
    currentIntervalDays: toDays({ milliseconds: currentInterval }),
  });
});

export default router;
