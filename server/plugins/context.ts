import { existsSync } from "node:fs";
import { resolve } from "node:path";

import Image from "../types/image";
import { downloadFile } from "../utils/download";
import { copyFileAsync, statAsync } from "../utils/fs/async";
import { logger } from "../utils/logger";
import { getFolderPartition, libraryPath } from "../utils/path";
import { extensionFromUrl, getExtension } from "../utils/string";

export async function importImageFromPath(path: string, name: string, thumbnail?: boolean) {
  logger.verbose(`Importing image from ${path}`);

  if (!existsSync(path)) {
    throw new Error(`No image at path ${path}`);
  }

  const ext = getExtension(path);
  const img = new Image(thumbnail ? `${name} (thumbnail)` : name);

  const newPath = resolve(
    getFolderPartition(libraryPath("images")),
    `${img._id}${ext}`
  );
  img.path = newPath;

  logger.verbose(`Storing image at ${newPath}`);
  await copyFileAsync(path, newPath);

  const { size } = await statAsync(newPath);
  img.meta.size = size;

  logger.verbose(`Created image ${img._id} at ${newPath}`);

  return img;
}

export async function createLocalImage(
  path: string,
  name: string,
  thumbnail?: boolean
): Promise<Image> {
  path = resolve(path);
  let img = await Image.getByPath(path);

  if (img) {
    return img;
  }

  logger.verbose(`Creating image from ${path}`);
  img = new Image(thumbnail ? `${name} (thumbnail)` : name);
  img.path = path;
  logger.verbose(`Created image ${img._id}`);

  return img;
}

export async function createImage(url: string, name: string, thumbnail?: boolean): Promise<Image> {
  logger.verbose(`Creating image from ${url}`);

  const img = new Image(name);
  if (thumbnail) {
    img.name += " (thumbnail)";
  }
  const ext = extensionFromUrl(url);
  const path = resolve(getFolderPartition(libraryPath("images")), `${img._id}${ext}`);
  await downloadFile(url, path);
  img.path = path;
  logger.verbose(`Created image ${img._id}`);

  return img;
}
