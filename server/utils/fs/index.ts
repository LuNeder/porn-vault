import { lstatSync } from "node:fs";

export function isDirectory(path: string): boolean {
  return lstatSync(path).isDirectory();
}
