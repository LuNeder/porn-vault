import { randomBytes } from "node:crypto";

export function randomString(length = 8): string {
  return randomBytes(length / 2).toString("hex");
}

export function generateHash(): string {
  return new Date().valueOf().toString(36) + randomString();
}
