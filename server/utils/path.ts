import { randomBytes } from "node:crypto";
import { join, resolve } from "node:path";

import { getConfig } from "../config";
import { mkdirpSync } from "./fs/async";

const configFolder = process.env.PV_CONFIG_FOLDER || process.cwd();
export const tempPath = process.env.CACHE_DIRECTORY ?? "tmp";

export function libraryPath(str: string): string {
  return resolve(getConfig().persistence.libraryPath, "library", str);
}

export function configPath(...paths: string[]): string {
  return resolve(join(configFolder, ...paths));
}

export function getFolderPartition(baseFolder: string): string {
  const newFolder = resolve(
    baseFolder,
    randomBytes(1).toString("hex"),
    randomBytes(1).toString("hex")
  );
  mkdirpSync(newFolder);
  return newFolder;
}
