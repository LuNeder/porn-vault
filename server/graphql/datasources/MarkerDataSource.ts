import Marker from "../../types/marker";
import { BatchActorLoader, BatchImageLoader, BatchLabelLoader } from "./loaders";

export class MarkerDataSource {
  async getActorsForMarker(marker: Marker) {
    return BatchActorLoader.load(marker._id);
  }

  async getLabelsForMarker(marker: Marker) {
    return BatchLabelLoader.load(marker._id);
  }

  async getThumbnailForMarker(thumbId: string) {
    return BatchImageLoader.load(thumbId);
  }

  clearAll() {}
}
