import { GraphQLError } from "graphql";

import { collections } from "../../database";
import { onMovieCreate } from "../../plugins/events/movie";
import { indexMovies, removeMovie } from "../../search/movie";
import { indexScenes } from "../../search/scene";
import LabelledItem from "../../types/labelled_item";
import Movie from "../../types/movie";
import MovieScene from "../../types/movie_scene";
import Scene from "../../types/scene";
import { handleError, logger } from "../../utils/logger";
import { normalizeDescription, normalizeName } from "../../utils/string";
import { Dictionary, isBoolean, isNumber, isString } from "../../utils/types";
import { clearCaches } from "../datasources";

type IMovieUpdateOpts = Partial<{
  name: string;
  description: string;
  releaseDate: number;
  frontCover: string;
  backCover: string;
  spineCover: string;
  favorite: boolean;
  bookmark: number | null;
  rating: number;
  scenes: string[];
  studio: string | null;
  customFields: Dictionary<string[] | boolean | string | null>;
}>;

async function runMoviePlugins(id: string): Promise<Movie | null> {
  const movie = await Movie.getById(id);

  if (!movie) {
    throw new Error("Movie not found");
  }

  logger.info(`Running plugin action event for '${movie.name}'`);

  const pluginResult = await onMovieCreate(movie, "movieCustom");

  const updatedMovie = await collections.movies.partialUpdate(movie._id, pluginResult.diff);
  await indexMovies([movie]);

  clearCaches();

  return updatedMovie;
}

export default {
  async runMoviePlugins(_: unknown, { id }: { id: string }): Promise<Movie> {
    const result = await runMoviePlugins(id);
    if (!result) {
      throw new Error("Movie not found");
    }
    return result;
  },

  async addMovie(_: unknown, args: { name: string; scenes: string[] }): Promise<Movie> {
    if (args.name.length === 0) {
      throw new GraphQLError("A movie name must not be empty", {
        extensions: {
          code: "BAD_USER_INPUT",
        },
      });
    }

    const movie = new Movie(args.name);

    if (args.scenes) {
      if (Array.isArray(args.scenes)) {
        await Movie.setScenes(movie, args.scenes);
      }
    }

    try {
      const { diff } = await onMovieCreate(movie);
      Object.assign(movie, diff);
    } catch (error) {
      handleError("Failed movieCreated event", error);
    }

    await collections.movies.upsert(movie._id, movie);
    await indexMovies([movie]);

    return movie;
  },

  async removeMovies(_: unknown, { ids }: { ids: string[] }): Promise<boolean> {
    for (const id of ids) {
      const movie = await Movie.getById(id);

      if (movie) {
        await Movie.remove(movie._id);
        await removeMovie(movie._id);
        await LabelledItem.removeByItem(movie._id);
        await MovieScene.removeByMovie(movie._id);
      }
    }

    clearCaches();

    return true;
  },

  async updateMovies(
    _: unknown,
    { ids, opts }: { ids: string[]; opts: IMovieUpdateOpts }
  ): Promise<Movie[]> {
    const updatedMovies = [] as Movie[];
    const updatedScenes = [] as string[];

    for (const id of ids) {
      const movie = await Movie.getById(id);

      if (movie) {
        const diff: Partial<Movie> = {};

        if (isString(opts.name)) {
          diff.name = normalizeName(opts.name);
        }

        if (isString(opts.description)) {
          diff.description = normalizeDescription(opts.description);
        }

        if (opts.studio !== undefined) {
          diff.studio = opts.studio;
        }

        if (isString(opts.frontCover)) {
          diff.frontCover = opts.frontCover;
        }

        if (isString(opts.backCover)) {
          diff.backCover = opts.backCover;
        }

        if (isString(opts.spineCover)) {
          diff.spineCover = opts.spineCover;
        }

        if (Array.isArray(opts.scenes)) {
          const oldRefs = await MovieScene.getByMovie(movie._id);
          const oldIds = oldRefs.map(x => x.scene);
          updatedScenes.push(...oldIds);

          await Movie.setScenes(movie, opts.scenes);
          updatedScenes.push(...opts.scenes);
        }

        if (isNumber(opts.bookmark) || opts.bookmark === null) {
          diff.bookmark = opts.bookmark;
        }

        if (isBoolean(opts.favorite)) {
          diff.favorite = opts.favorite;
        }

        if (isNumber(opts.rating)) {
          diff.rating = opts.rating;
        }

        if (opts.releaseDate !== undefined) {
          diff.releaseDate = opts.releaseDate;
        }

        if (opts.customFields) {
          for (const key in opts.customFields) {
            const value = opts.customFields[key] !== undefined ? opts.customFields[key] : null;
            logger.debug(`Set scene custom.${key} to ${JSON.stringify(value)}`);
            opts.customFields[key] = value;
          }
          diff.customFields = opts.customFields;
        }

        const updatedMovie = await collections.movies.partialUpdate(movie._id, diff);
        updatedMovies.push(updatedMovie);
      }
    }

    clearCaches();
    await indexMovies(updatedMovies);

    if (updatedScenes.length) {
      const scenes = await Scene.getBulk(updatedScenes);
      await indexScenes(scenes);
    }

    return updatedMovies;
  },
};
