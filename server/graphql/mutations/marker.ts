import { GraphQLError } from "graphql";

import { getConfig } from "../../config/index";
import { collections } from "../../database";
import { extractLabels } from "../../extractor";
import { indexMarkers, removeMarker } from "../../search/marker";
import Actor from "../../types/actor";
import LabelledItem from "../../types/labelled_item";
import Marker from "../../types/marker";
import Scene from "../../types/scene";
import { logger } from "../../utils/logger";
import { normalizeName } from "../../utils/string";
import { isBoolean, isNumber, isString } from "../../utils/types";
import { clearCaches } from "../datasources";

interface ICreateMarkerArgs {
  scene: string;
  name: string;
  time: number;
  rating?: number | null;
  favorite?: boolean | null;
  bookmark?: number | null;
  labels?: string[] | null;
  actors?: string[] | null;
}

type IMarkerUpdateOpts = Partial<{
  favorite: boolean;
  bookmark: number;
  actors: string[];
  name: string;
  rating: number;
  labels: string[];
}>;

export default {
  async updateMarkers(
    _: unknown,
    { ids, opts }: { ids: string[]; opts: IMarkerUpdateOpts }
  ): Promise<Marker[]> {
    const updatedMarkers: Marker[] = [];

    for (const id of ids) {
      const marker = await Marker.getById(id);

      if (marker) {
        const diff: Partial<Marker> = {};

        if (isString(opts.name)) {
          diff.name = normalizeName(opts.name);
        }

        if (Array.isArray(opts.labels)) {
          await Marker.setLabels(marker, opts.labels);
        }

        if (Array.isArray(opts.actors)) {
          await Marker.setActors(marker, opts.actors);
        }

        if (isNumber(opts.bookmark) || opts.bookmark === null) {
          diff.bookmark = opts.bookmark;
        }

        if (isBoolean(opts.favorite)) {
          diff.favorite = opts.favorite;
        }

        if (isNumber(opts.rating)) {
          diff.rating = opts.rating;
        }

        const updatedMarker = await collections.markers.partialUpdate(marker._id, diff);
        updatedMarkers.push(updatedMarker);
      }
    }

    clearCaches();
    await indexMarkers(updatedMarkers);

    return updatedMarkers;
  },

  async createMarker(
    _: unknown,
    { scene, name, time, rating, favorite, bookmark, labels, actors }: ICreateMarkerArgs
  ): Promise<Marker> {
    const _scene = await Scene.getById(scene);
    if (!_scene) {
      throw new GraphQLError("Scene not found", {
        extensions: {
          code: "BAD_USER_INPUT",
        },
      });
    }

    if (name.length === 0) {
      throw new GraphQLError("A marker name must not be empty", {
        extensions: {
          code: "BAD_USER_INPUT",
        },
      });
    }

    const marker = new Marker(name, scene, time);

    if (isNumber(rating)) {
      if (rating < 0 || rating > 10) {
        throw new Error("BAD_REQUEST");
      }
      marker.rating = rating;
    }

    if (isBoolean(favorite)) {
      marker.favorite = favorite;
    }

    if (isNumber(bookmark)) {
      marker.bookmark = bookmark;
    }

    await collections.markers.upsert(marker._id, marker);

    // Extract labels
    const existingLabels = labels || [];
    const extractedLabels = await extractLabels(marker.name);
    existingLabels.push(...extractedLabels);
    logger.verbose(`Found ${extractedLabels.length} labels in marker name`);
    await Marker.setLabels(marker, existingLabels);

    // Set actors
    if (actors) {
      await Marker.setActors(marker, actors);

      const config = getConfig();
      if (config.matching.applyActorLabels.includes("plugin:marker:create")) {
        for (const actorId of actors) {
          const actor = await Actor.getById(actorId);

          if (actor) {
            const actorLabels = await Actor.getLabels(actor);
            await Marker.addLabels(
              marker,
              actorLabels.map((l) => l._id)
            );
          }
        }
      }
    }

    const diff = await Marker.createMarkerThumbnail(marker);
    Object.assign(marker, diff);

    await collections.markers.upsert(marker._id, marker);
    await indexMarkers([marker]);

    return marker;
  },

  async removeMarkers(_: unknown, { ids }: { ids: string[] }): Promise<boolean> {
    for (const id of ids) {
      const marker = await Marker.getById(id);

      if (marker) {
        await Marker.remove(marker._id);
        await removeMarker(marker._id);
        await LabelledItem.removeByItem(marker._id);
      }
    }

    clearCaches();

    return true;
  },
};
