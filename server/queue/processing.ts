import { getImageDimensions } from "../binaries/imagemagick";
import { getConfig } from "../config";
import { collections } from "../database";
import { indexImages } from "../search/image";
import { indexScenes } from "../search/scene";
import Image from "../types/image";
import Scene from "../types/scene";
import { statAsync } from "../utils/fs/async";
import { formatMessage, handleError, logger } from "../utils/logger";

export interface ISceneProcessingItem {
  _id: string;
}

let processing = false;

export function setProcessingStatus(value: boolean): void {
  processing = value;
}

export function isProcessing(): boolean {
  return processing;
}

export function removeSceneFromQueue(id: string): Promise<void> {
  logger.debug(`Removing ${id} from processing queue`);
  return collections.processing.remove(id);
}

export function getLength(): Promise<number> {
  return collections.processing.count();
}

export async function getHead(): Promise<ISceneProcessingItem | null> {
  logger.debug("Getting queue head");
  const item = await collections.processing.getHead();
  return item;
}

export function enqueueScene(_id: string): Promise<ISceneProcessingItem> {
  logger.debug(`Adding scene "${_id}" to processing queue`);
  return collections.processing.upsert(_id, { _id });
}

async function getQueueHead(): Promise<Scene | null> {
  let scene: Scene | null = null;

  do {
    const queueHead = await getHead();
    if (!queueHead) {
      logger.silly("Empty queue, returning null");
      return null;
    }

    scene = await Scene.getById(queueHead._id);
    if (!scene) {
      logger.warn(
        `Scene ${queueHead._id} doesn't exist (anymore?), deleting from processing queue`
      );
      await collections.processing.remove(queueHead._id);
    }
  } while (!scene && (await collections.processing.count()) > 0);

  return scene;
}

export async function tryStartProcessing(): Promise<void> {
  if (isProcessing()) {
    return;
  }

  logger.verbose("Checking processing queue length");

  const config = getConfig();
  const queueLen = await getLength();

  if (!queueLen) {
    logger.info("No more videos to process");
    return;
  }

  logger.info("Starting processing");
  setProcessingStatus(true);

  try {
    let queueHead = await getQueueHead();

    while (queueHead) {
      try {
        logger.verbose(`Processing "${queueHead.path}"`);

        const data: Partial<Scene> = { processed: true };

        const images: Image[] = [];
        const thumbs: Image[] = [];

        if (!queueHead.preview) {
          if (config.processing.generatePreviews) {
            const preview = await Scene.generatePreview(queueHead);

            if (preview) {
              const image = new Image(`${queueHead.name} (preview)`);
              const stats = await statAsync(preview);
              image.path = preview;
              image.scene = queueHead._id;
              image.meta.size = stats.size;

              const dims = await getImageDimensions(image.path);
              image.meta.dimensions.width = dims.width;
              image.meta.dimensions.height = dims.height;

              thumbs.push(image);
              data.preview = image._id;
            } else {
              logger.error("Error generating preview.");
            }
          } else {
            logger.verbose(
              "Skipping preview generation, because config.processing.generatePreviews is false"
            );
          }
        }

        logger.debug("Updating scene data");

        const scene = await Scene.getById(queueHead._id);
        if (!scene) {
          continue;
        }

        Object.assign(scene, data);

        await collections.scenes.upsert(scene._id, scene);
        await indexScenes([scene]);

        for (const image of images) {
          logger.silly(`New image: ${formatMessage(image)}`);

          await collections.images.upsert(image._id, image);
          await indexImages([image]);

          const actors = await Scene.getActors(scene);
          const labels = await Scene.getLabels(scene);

          await Image.setActors(
            image,
            actors.map((a) => a._id)
          );
          await Image.setLabels(
            image,
            labels.map((a) => a._id)
          );
        }

        for (const thumb of thumbs) {
          logger.silly(`New thumbnail: ${formatMessage(thumb)}`);
          await collections.images.upsert(thumb._id, thumb);
        }

        await removeSceneFromQueue(queueHead._id);
      } catch (error) {
        handleError("Processing error", error);
        logger.debug("Removing item from queue");
        await removeSceneFromQueue(queueHead._id);
      }

      queueHead = await getQueueHead();
    }

    logger.info("Processing successful");
  } catch (error) {
    handleError("Processing error", error);
  } finally {
    setProcessingStatus(false);
  }
}
