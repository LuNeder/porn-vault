import { deepKeys } from "dot-prop";

import german from "./de.json" assert { type: "json" };
import english from "./en.json" assert { type: "json" };
import french from "./fr.json" assert { type: "json" };

const englishKeys = [...deepKeys(english)];

for (const lang of [german /* french */]) {
  const langKeys = deepKeys(lang);

  let errors = 0;

  for (const langProp of langKeys) {
    if (!englishKeys.includes(langProp)) {
      console.error(
        `[${lang.alpha2}] Prop missing in English or obsolete: ${langProp}`
      );
      errors++;
    }
  }

  for (const prop of englishKeys) {
    if (!langKeys.includes(prop)) {
      console.error(
        `[${lang.alpha2}] Prop missing in ${lang.currentLanguage} or obsolete in English: ${prop}`
      );
      errors++;
    }
  }

  console.error(
    `[${lang.alpha2}] Progress: ${(
      ((englishKeys.length - errors) / englishKeys.length) *
      100
    ).toFixed(0)}%`
  );
}
