# Using plugins

## Plugins repository

You can find some plugins to use at [https://gitlab.com/porn-vault/plugins](https://gitlab.com/porn-vault/plugins).

## Register plugin

To use a plugin, first it needs to be registered in the `config.json/yaml`.
In this case, the plugin source (.mjs file) is stored in a folder called `plugins/`.

> If using a relative path, it will be resolved relatively to the config file.

JSON

```javascript
{
  "plugins": {
    "register": {
      "you_can_choose_this_name": {
        "path": "./plugins/myplugin.mjs"
      }
    }
  }
}
```

YAML

```yaml
---
plugins:
  register:
    you_can_choose_this_name:
      path: "./plugins/myplugin.mjs"
```

## Run registered plugin

Run the registered plugin every time an actor is created (see 'Events' for more info on events).
Every event accepts a list of plugins to run _in series_.
Plugin results will be **aggregated** into one object and then processed.
That means the results of the last plugin to run have higher priority.

JSON

```javascript
{
  "plugins": {
    "events": {
      "actorCreated": ["you_can_choose_this_name"]
    }
  }
}
```

YAML

```yaml
---
plugins:
  events:
    actorCreated:
      - you_can_choose_this_name
```

## Events

### Actor

- **`actorCreated`**
- - Ran when an actor is created
- **`actorCustom`**
- - Ran when the user runs the plugin manually in the interface

### Scene

- **`sceneCreated`**
- - Ran when an actor is created
- **`sceneCustom`**
- - Ran when the user runs the plugin manually in the interface

### Movie

- **`movieCreated`**
- - Ran when an actor is created

## Additional arguments

Additional arguments can be passed to plugins.

JSON

```javascript
{
  "plugins": {
    "register": {
      "someScraper": {
        "path": "./plugins/imaginaryapi.mjs",
        "args": {
          "verbose": true
        }
      }
    }
  }
}
```

YAML

```yaml
---
plugins:
  register:
    someScraper:
      path: "./plugins/imaginaryapi.mjs"
      args:
        verbose: true
```
