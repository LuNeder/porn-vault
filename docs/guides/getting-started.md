# Getting started

::: info
This page is tailored for PV version 0.30.0+.
Docker images may not be available just yet.
:::

## Not using Docker?

Try [building from source](/guides/build-from-source).

> Standalone releases are planned, but not available right now

## Docker

The image can be found at:

- Stable: `registry.gitlab.com/porn-vault/porn-vault:alpine-latest`
- Unstable: `registry.gitlab.com/porn-vault/porn-vault:alpine-edge`

> Debian builds are also available - just change `alpine` to `debian`

::: warning
If you're running on a Raspberry Pi (or other device with an architecture different from x86_64), you'll need to [build the image yourself](/guides/build-container).
:::

## Docker Compose

```yml
version: "3"
services:
  porn-vault:
    image: registry.gitlab.com/porn-vault/porn-vault:alpine-edge
    container_name: porn-vault
    volumes:
      - "/path/to/config:/config"
      - "/path/to/videos:/videos"
      - "/path/to/images:/images"
    ports:
      - "3000:3000"
    restart: unless-stopped
    networks:
      - porn-vault-net
networks:
  porn-vault-net:
```
