# Building the Container

::: info
This should be done on the device you want to run porn-vault on. If doing from a device with a different architecture, additional steps for cross-compilation would be required [(see further down)](#cross-compiling-izzy).
:::

Cloning the main application is straightforward, just run:

```sh
git clone https://gitlab.com/porn-vault/porn-vault
cd porn-vault
docker build . -f docker/Dockerfile.alpine -t pv:latest
```

::: info
You can also build the debian container instead, just change `alpine` to `debian` above
:::

You'll also need to build izzy. Install the [Rust toolchain](https://www.rust-lang.org/tools/install) with default options. Then, depending on your architecture, run:

### x86_64

```sh
git clone https://gitlab.com/porn-vault/izzy
cd izzy
rustup target add x86_64-unknown-linux-musl
cargo build --release --target=x86_64-unknown-linux-musl
```

### arm64 (Raspberry Pi, Apple Silicon, etc)

```sh
git clone https://gitlab.com/porn-vault/izzy
cd izzy
rustup target add aarch64-unknown-linux-musl
cargo build --release --target=aarch64-unknown-linux-musl
```

::: info
If using the debian container instead, you shouldn't need to install the extra `musl` targets above and should be able to just do `cargo build --release` instead
:::

Copy the generated izzy binary to the config folder, the same place your config.json will be. Then you should be able to use the Docker Compose normally.

## Cross-compiling izzy

Install cargo cross:

```sh
cargo install cross
```

Then install the toolchain for the platform you want to compile to, and compile it using cross.

For instance, to compile to arm64 from a x86_64 computer:

```sh
rustup target add aarch64-unknown-linux-musl
cross build -r --target aarch64-unknown-linux-musl
```
